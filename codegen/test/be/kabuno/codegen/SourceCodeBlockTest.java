package be.kabuno.codegen;

import org.junit.Before;
import org.junit.Test;

import java.util.stream.Collectors;

/**
 * Test case for {@link SourceCodeBlock}.
 * @author Laurens Devos
 */
public class SourceCodeBlockTest {

    SourceCodeBlock cb;

    @Before
    public void setUp() throws Exception {
        cb = new SourceCodeBlock();
    }

    @Test
    public void test1() throws Exception {
        cb.openBlock("for (int i = 0; i < size; ++i)");
        cb.putln("do something boys;");
        cb.closeBlock();

        String result = cb.stream().collect(Collectors.joining());
        System.out.println(result);
    }
}