package be.kabuno.codegen;

import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static be.kabuno.codegen.SourceWriter.NEWLINE;
import static org.junit.Assert.*;

/**
 * Test case for {@link SourceImports}.
 * @author Laurens Devos
 */
public class SourceImportsTest {

    SourceImports si;

    @Before
    public void setUp() throws Exception {
        si = new SourceImports("be.kabuno.activePackage");
    }

    @Test
    public void test1() throws Exception {
        si.addImport("be.kabuno.Test");
        si.addImport("com.other.Test");

        assertEquals("Test", si.getType("be.kabuno.Test"));
        assertEquals("com.other.Test", si.getType("com.other.Test"));
    }

    @Test
    public void test2() {
        assertEquals("Imported", si.getType("be.kabuno.activePackage.Imported"));
    }

    @Test
    public void test3() throws Exception {
        si.addImport("be.kabuno.*");
        assertEquals("Test", si.getType("be.kabuno.Test"));
    }

    @Test
    public void test4() throws Exception {
        assertEquals("String", si.getType("java.lang.String"));
    }

    @Test
    public void name4() throws Exception {
        si.addImport("be.kabuno.List");
        si.addImport("be.kabuno.proxywrap.*");
        assertEquals("List<String>", si.getType("java.lang.List<String>"));
        assertEquals("List<String>", si.getType("be.kabuno.List<String>"));
        assertEquals("WrapList<String>", si.getType("be.kabuno.proxywrap.WrapList<String>"));
        assertEquals("WrapList<String>", si.getType("be.kabuno.proxywrap.WrapList<java.lang.String>"));
        assertEquals("WrapList<String, List, A>", si.getType("be.kabuno.proxywrap.WrapList<java.lang.String, be.kabuno.List, be.kabuno.proxywrap.A>"));
        assertEquals("WrapList<String, List, A>", si.getType("be.kabuno.proxywrap.WrapList<java.lang.String    , be.kabuno.List, be.kabuno.proxywrap.A>"));
        assertEquals("WrapList<List<List>>", si.getType("be.kabuno.proxywrap.WrapList<java.lang.List<be.kabuno.List>>"));
    }

    @Test
    public void test5() throws Exception {
        //si = new SourceImports("be.kuleuven.swop.domain");
        si.addImport("be.kabuno.proxywrap.*");
        si.addImport("be.kuleuven.swop.domain.bugreport.BugReportAccess");
        String result = si.getType("be.kabuno.proxywrap.WrapList<be.kuleuven.swop.domain.bugreport.BugReportAccess>");
        assertEquals("WrapList<BugReportAccess>", result);
    }

    @Test
    public void testIterator() throws Exception {
        si.addImport("be.kabuno.BTest");
        si.addImport("be.kabuno.ATest");

        String s = si.stream().collect(Collectors.joining());
        assertEquals("import be.kabuno.ATest;" + NEWLINE +
                "import be.kabuno.BTest;" + NEWLINE, s);
    }

    @Test()
    public void testIterator_fail() throws Exception {
        si.addImport("be.BTest");

        Iterator<String> iterator = si.iterator();
        assertEquals("import ", iterator.next());
        assertEquals("be.BTest", iterator.next());
        assertEquals(";", iterator.next());
        assertEquals(NEWLINE, iterator.next());
        assertFalse(iterator.hasNext());

        try {
            iterator.next();
            fail();
        } catch (NoSuchElementException ignored) {}
    }

    @Test
    public void testOrdering() {
        si.addImport("zed.low.order.*");
        si.addImport("java.util.Collections");
        si.addImport("java.util.Arrays");

        String s = si.stream().collect(Collectors.joining());
        assertEquals("import java.util.Arrays;" + NEWLINE +
                "import java.util.Collections;" + NEWLINE +
                "import zed.low.order.*;" + NEWLINE, s);
    }
}