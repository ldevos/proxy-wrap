package be.kabuno.codegen;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.StringWriter;

import static be.kabuno.codegen.SourceWriter.NEWLINE;
import static be.kabuno.codegen.SourceWriter.TYPE;

/**
 * Test for {@link SourceWriter}.
 *
 * @author Laurens Devos
 */
public class SourceWriterTest {

    StringWriter stringWriter;
    BufferedWriter writer;

    @Before
    public void setUp() throws Exception {
        stringWriter = new StringWriter();
        writer = new BufferedWriter(stringWriter);
    }

    @Test
    public void test() throws Exception {
        SourceFile sf = new SourceFile("be.kabuno", "Bob");

        sf.imports.addImport("java.util.Iterable");
        sf.imports.addImport("be.kabuno.Alice");
        sf.imports.addImport("be.kabuno.all.*");

        sf.type.setGenerics("A, B, C");
        sf.type.setModifiers("public abstract class");
        sf.type.extendsClause.put("extends Object implements ", TYPE, "java.util.Iterable<BobParts>");
        sf.type.docs.put(NEWLINE, "Hello world! Bob", NEWLINE, NEWLINE, "@author Bob Himself");
        sf.type.docs.putln(" copyright");
        sf.type.addField("public static final", "java.lang.String", "myString");
        sf.type.addField("private", "be.kabuno.Alice", "alice");
        sf.type.addField("private", "be.kabuno.Other", "other");
        sf.type.addField("private static final", "be.kabuno.all.Whatever", "$null_ = null");

        SourceMethod method = new SourceMethod("Bob");
        method.setModifiers("public");
        method.addThrowType("java.lang.Exception");
        method.addParam("be.kabuno.Alice", "alice");
        method.addParam("be.kabuno.Other", "other");
        method.body.putln("this.alice = alice;");
        method.body.putln("this.other = other;");
        method.body.openBlock("if (cond)");
        method.body.putln("throw new ", TYPE, "java.lang.Exception", "(\"not right\");");
        method.body.closeBlock();
        method.docs.putln(NEWLINE, "this is my constructor documentation");
        sf.type.addMember(method);

        method = new SourceMethod("toImplement");
        method.setModifiers("public abstract");
        method.addParam("be.kabuno.all.Other", "otherConflict");
        method.setReturnType("java.lang.String");
        method.addThrowType("be.kabuno.all.NoSuchExceptionException");
        method.addThrowType("org.lib.OutOfExceptionException");
        method.addThrowType("MetaException");
        sf.type.addMember(method);

        SourceWriter.write(writer, sf, sf.imports);
        writer.flush();
        System.out.println(stringWriter.toString());
    }

    @Test
    public void test2() throws Exception {
        SourceCodeBlock block = new SourceCodeBlock();
        block.openBlock("for (String s : strings)");
        block.putln("do something with s;");
        block.openBlock("if (condition)");
        block.putln("something special;");
        block.closeBlock();
        block.closeBlock();

        SourceWriter.write(writer, block, new SourceImports(""));
        writer.flush();
        System.out.println(stringWriter.toString());
    }

    @Test
    public void testMethod() throws Exception {
        SourceImports imports = new SourceImports("");
        imports.addImport("be.kabuno.Context");
        imports.addImport("be.kabuno.SenselessMoveException");
        SourceMethod method = new SourceMethod("moveTo");
        method.setReturnType("be.kabuno.Context");
        method.addParam("int", "x");
        method.addParam("int", "y");
        method.addParam("be.kabuno.Context", "ctx");
        method.addThrowType("be.kabuno.SenselessMoveException");
        method.addThrowType("java.lang.Exception");
        method.body.putln("this.x = x;");
        method.body.putln("this.y = y;");
        method.body.openBlock("if (cond)");
        method.body.putln("do something please;");
        method.body.closeBlock();
        method.body.putln("return ctx;");

        SourceWriter.write(writer, method, imports);
        writer.flush();
        System.out.println(stringWriter.toString());
    }
}