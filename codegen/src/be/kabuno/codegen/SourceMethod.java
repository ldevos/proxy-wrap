package be.kabuno.codegen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;

import static be.kabuno.codegen.SourceWriter.*;

/**
 * A source code method.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceMethod implements SourceIterable {

    public final String name;
    public final SourceDocs docs = new SourceDocs();
    public final SourceCodeBlock body = new SourceCodeBlock();
    public final SourceCodeBlock returnType = new SourceCodeBlock();

    private String modifiers = "";
    private ArrayList<String> params = new ArrayList<>();
    private ArrayList<String> throwz = new ArrayList<>();

    /** A source buffer for annotation code. */
    public final SourceBuffer annotations = new SourceBuffer();

    /** Construct a new source method. */
    public SourceMethod(String name) {
        this.name = name;
    }

    /** Set the modifiers of this method. */
    public void setModifiers(String modifiers) {
        this.modifiers = modifiers;
    }

    /** Set the return type of this method. */
    public void setReturnType(String qualifiedReturnType) {
        returnType.put(TYPE, qualifiedReturnType, " ");
    }

    /** Add a parameter to the method. */
    public void addParam(String qualifiedType, String paramName) {
        addParamRaw(paramName, TYPE, qualifiedType);
    }

    /** Add a parameter to the method. The argument type can be a self-defined array of pieces. */
    public void addParamRaw(String paramName, String... typePieces) {
        if (!params.isEmpty())
            params.add(", ");

        Arrays.stream(typePieces).forEach(params::add);
        params.add(" ");
        params.add(paramName);
    }

    /** Add a throws type. */
    public void addThrowType(String qualifiedName) {
        if (!throwz.isEmpty())
            throwz.add(", ");

        throwz.add(TYPE);
        throwz.add(qualifiedName);
    }

    /** Add throws type. */
    public void addThrowType(Class<? extends Exception> type) {
        addThrowType(type.getCanonicalName());
    }

    @Override
    public Stream<String> stream() {
        ArrayList<Stream<String>> streams = new ArrayList<>();

        if (!docs.isEmpty())
            streams.add(docs.stream());

        if (!annotations.isEmpty())
            streams.add(annotations.stream());

        if (!modifiers.isEmpty())
            streams.add(Stream.of(modifiers, " "));

        if (!returnType.isEmpty())
            streams.add(returnType.stream());

        streams.add(Stream.of(name, "("));
        streams.add(params.stream());
        streams.add(Stream.of(")"));

        if (!throwz.isEmpty()) {
            streams.add(Stream.of(" throws "));
            streams.add(throwz.stream());
        }

        if (body.isEmpty()) {
            streams.add(Stream.<String>of(";", NEWLINE));
        } else {
            streams.add(Stream.<String>of(" {", INDENT, NEWLINE));
            streams.add(body.stream());
            streams.add(Stream.of(UN_INDENT, "}", NEWLINE));
        }

        return streams.stream().flatMap(Function.identity());
    }
}
