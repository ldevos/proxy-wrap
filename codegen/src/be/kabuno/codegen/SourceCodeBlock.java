package be.kabuno.codegen;

import static be.kabuno.codegen.SourceWriter.*;

/**
 * A block of code.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceCodeBlock extends SourceBuffer {

    /** Open a new code block. */
    public void openBlock(String... os) {
        put(os);
        put(" {", INDENT, NEWLINE);
    }

    /** Close a code block. */
    public void closeBlock() {
        putln(UN_INDENT, "}");
    }
}
