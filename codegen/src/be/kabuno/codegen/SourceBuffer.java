package be.kabuno.codegen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import static be.kabuno.codegen.SourceWriter.*;

/**
 * A source code buffer to which can be written.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceBuffer implements SourceIterable {

    private ArrayList<String> pieces = new ArrayList<>();
    private StringBuilder activePiece = new StringBuilder();

    /**
     * Add a piece to this buffer. A newline is added at the end.
     *
     * @param os The object to write.
     */
    public void putln(String... os) {
        put(os);
        flush();
        pieces.add(NEWLINE);
    }

    /**
     * Add a piece to this buffer.
     *
     * @param os The objects to add.
     */
    public void put(String... os) {
        putIt(Arrays.asList(os));
    }

    /**
     * Add all the pieces in this stream.
     * @param stream A stream of pieces.
     */
    public void put(Stream<String> stream) {
        putIt(stream::iterator);
    }

    private void putIt(Iterable<String> iterable) {
        boolean splitOnType = false;
        for (Object o : iterable) {
            if (splitOnType) {
                flush();
                pieces.add(TYPE);
                pieces.add(o.toString()); // this should be the qualified name
                splitOnType = false;
            } else if (o == TYPE) {
                splitOnType = true;
            } else if (o == NEWLINE || o == INDENT || o == UN_INDENT) {
                flush();
                pieces.add(o.toString());
            } else {
                activePiece.append(o);
            }
        }
    }

    private void flush() {
        if (activePiece.length() > 0) {
            pieces.add(activePiece.toString());
            activePiece.setLength(0);
        }
    }

    @Override
    public Stream<String> stream() {
        if (activePiece.length() > 0)
            return Stream.concat(pieces.stream(), Stream.of(activePiece.toString()));
        else return pieces.stream();
    }

    /**
     * Return whether pieces have been written to this buffer.
     *
     * @return True if the buffer is empty, false otherwise.
     */
    public boolean isEmpty() {
        return pieces.isEmpty() && activePiece.length() == 0;
    }
}
