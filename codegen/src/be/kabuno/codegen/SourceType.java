package be.kabuno.codegen;

import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

import static be.kabuno.codegen.SourceWriter.*;

/**
 * A class or interface source code iterator.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceType implements SourceIterable {

    public final String name;
    public final SourceDocs docs = new SourceDocs();

    private String modifiers = "public class";
    private String generics = "";
    private ArrayList<SourceIterable> members = new ArrayList<>();

    /** A source buffer for annotation code. */
    public final SourceBuffer annotations = new SourceBuffer();

    /** A source buffer for the extends clause. */
    public final SourceBuffer extendsClause = new SourceBuffer();

    /**
     * Construct a new source class with the given name.
     * @param name The name of the class.
     */
    public SourceType(String name) {
        this.name = name;
    }

    /**
     * Set the modifiers of this class. "public class" by default.
     * The modifiers also include the keywords 'class' or 'interface'.
     */
    public void setModifiers(String modifiers) {
        this.modifiers = modifiers;
    }

    /**
     * Set the type parameters for this class.
     * @param generics The type parameters as a string, e.g. "A, G"
     */
    public void setGenerics(String generics) {
        this.generics = generics;
    }

    /** Add a new field to this class. */
    public void addField(String access, String type, String name) {
        members.add(new Field(access, type, name));
    }

    /** Add a new member, e.g. a {@link SourceMethod method}. */
    public void addMember(SourceIterable member) {
        members.add(member);
    }

    @Override
    public Stream<String> stream() {
        ArrayList<Stream<String>> streams = new ArrayList<>();

        if (!docs.isEmpty())
            streams.add(docs.stream());

        if (!annotations.isEmpty())
            streams.add(annotations.stream());

        streams.add(Stream.of(modifiers, " ", name));

        if (!generics.isEmpty())
            streams.add(Stream.of("<", generics, "> "));

        if (!extendsClause.isEmpty()) {
            streams.add(Stream.of(" "));
            streams.add(extendsClause.stream());
        }

        streams.add(Stream.of(" {", INDENT, NEWLINE));
        streams.add(members.stream().flatMap(SourceIterable::stream));
        streams.add(Stream.of(UN_INDENT, "}", NEWLINE));

        return streams.stream().flatMap(Function.identity());
    }

    /**
     * A class field method iterator.
     */
    public static final class Field implements SourceIterable {

        private final String access;
        private final String type;
        private final String name;

        public Field(String access, String type, String name) {
            this.access = Objects.requireNonNull(access);
            this.type = Objects.requireNonNull(type);
            this.name = Objects.requireNonNull(name);
        }

        @Override
        public Stream<String> stream() {
            if (access.isEmpty()) return Stream.of(TYPE, type, " ", name, ";", NEWLINE);
            else                  return Stream.of(access, " ", TYPE, type, " ", name, ";", NEWLINE);
        }
    }
}
