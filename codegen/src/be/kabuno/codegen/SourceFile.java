package be.kabuno.codegen;

import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

import static be.kabuno.codegen.SourceWriter.NEWLINE;

/**
 * A base class for generated source files.
 * A source file contains one class with methods, has a package declaration and has imports.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceFile implements SourceIterable {

    public final String pkg, className;

    public final SourceImports imports;
    public final SourceType type; // only one class per file for now

    /**
     * Create a new source file.
     *
     * @param pkg The package of the class.
     * @param typeName The name of the public class/interface in this file.
     */
    public SourceFile(String pkg, String typeName) {
        this.pkg = Objects.requireNonNull(pkg);
        this.className = Objects.requireNonNull(typeName);

        imports = new SourceImports(pkg);
        type = new SourceType(typeName);
    }

    @Override
    public Stream<String> stream() {
        ArrayList<Stream<String>> streams = new ArrayList<>();
        if (!pkg.isEmpty())
            streams.add(Stream.of("package ", pkg, ";", NEWLINE, NEWLINE));
        streams.add(imports.stream());
        streams.add(Stream.of(NEWLINE));
        streams.add(type.stream());
        streams.add(Stream.of(NEWLINE));
        return streams.stream().flatMap(Function.identity());
    }

}
