package be.kabuno.codegen;

import java.util.Iterator;
import java.util.stream.Stream;

/**
 * An iterable for source code.
 * <p> A source iterator iterates over <i>pieces</i>. That's just a name for bits of code.</p>
 * <p> A stream of pieces can have <i>indicators</i>. These indicators can be found as static
 * fields in {@link SourceWriter}, and are used for newlines, indentation, and type
 * replacement.</p>
 *
 * @author Laurens Devos
 * @since 1.0
 */
public interface SourceIterable extends Iterable<String> {

    /**
     * Iterate over the string pieces in this buffer.
     *
     * @return An iterator of string pieces.
     */
    @Override
    default Iterator<String> iterator() {
        return stream().iterator();
    }

    /**
     * Get a stream of the string pieces in this buffer.
     *
     * <p> Convention: source iterables (optionally) end in a newline, but
     * don't start with a newline.</p>
     *
     * @return A stream of string pieces.
     */
    Stream<String> stream();
}
