package be.kabuno.codegen;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * TODO docs
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceWriter {

    /**
     * Newline indicator.
     * <p> When a {@link SourceIterable#stream() stream of pieces} is
     * {@link #write(BufferedWriter, SourceIterable, SourceImports)} written to a writer,
     * <tt>NEWLINES</tt> are replaced by the system-specific line separator (the
     * one offered by {@link BufferedWriter#newLine()} is used).</p>
     */
    public static final String NEWLINE = " NEWLINE\n";

    /**
     * Indent indicator.
     * <p> The indent counter is incremented by one when an INDENT piece is seen in
     * a {@link SourceIterable#stream() stream of pieces}.</p>
     */
    public static final String INDENT = " INDENT ";

    /**
     * Un-indent indicator.
     * <p> The un-indent counter is decremented by one when an UN_INDENT piece is seen in
     * a {@link SourceIterable#stream() stream of pieces}.</p>
     */
    public static final String UN_INDENT = " UN-INDENT ";

    /**
     * Next-piece-is-type indicator.
     * <p> When this piece is encountered in a {@link SourceIterable#stream()}, the next
     * token is parsed as a type, i.e. a fully qualified name that can potentially be
     * simplified by a {@link SourceImports#getType(String)} call.</p>
     */
    public static final String TYPE = "TYPE";

    /**
     * Use this indicator to indicate that this piece should be replaced by something else.
     * This is not actually used by the writer itself, but can be used by other classes.
     */
    public static final String REPLACE = "REPLACE";

    private static final String TAB = "    ";

    /**
     * Overloaded version of {@link #write(BufferedWriter, SourceIterable, SourceImports)} that uses
     * {@link SourceFile#imports} as the last parameter.
     *
     * @param writer The writer to write to.
     * @param file The source file to write to the writer.
     * @throws IOException An I/O exception caused by the writer.
     */
    public static void write(BufferedWriter writer, SourceFile file) throws IOException {
        write(writer, file, file.imports);
    }

    /**
     * Write a source iterable to a writer.
     *
     * @param writer The writer to write to.
     * @param source The source code to write to the writer.
     * @param imports The imports to use to replace fully qualified type names to
     *                simplified names (i.e. the pieces that follow a {@link #TYPE}
     *                piece.
     * @throws IOException An I/O exception caused by the writer.
     */
    public static void write(BufferedWriter writer, SourceIterable source, SourceImports imports) throws IOException {
        String indentPrefix = "";
        boolean parseType = false;
        int newline = 0;

        for (String s : source) {
            switch (s) {
            case INDENT:
                indentPrefix += TAB;
                break;

            case UN_INDENT:
                if (!indentPrefix.isEmpty())
                    indentPrefix = indentPrefix.substring(TAB.length());
                break;

            case NEWLINE:
                writer.newLine();
                newline++;
                break;

            case TYPE:
                parseType = true;
                break;

            default:
                if (newline > 0) {
                    newline = 0;
                    writer.write(indentPrefix);
                }
                if (parseType) {
                    writer.write(imports.getType(s));
                    parseType = false;
                } else {
                    writer.write(s);
                }
                break;
            }
        }
    }
}
