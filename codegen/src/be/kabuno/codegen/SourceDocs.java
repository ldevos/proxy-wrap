package be.kabuno.codegen;

import java.util.stream.Stream;

import static be.kabuno.codegen.SourceWriter.NEWLINE;
import static be.kabuno.codegen.SourceWriter.write;

/**
 * Class that represents javadoc-style documentation.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceDocs extends SourceBuffer {

    /** Kinds of documentation. */
    public enum Type {
        DOUBLE_SLASH("// ", "//", ""),
        JAVADOC("/** ", " *", " */"),
        BLOCK("/* ", " *", " */"), ;

        public final String open, prefix, close;

        Type(String open, String prefix, String close) {
            this.open = open;
            this.prefix = prefix;
            this.close = close;
        }
    }

    private Type docType = Type.JAVADOC;

    /**
     * Set the comment type.
     */
    public void setType(Type docType) {
        this.docType = docType;
    }

    @Override
    public Stream<String> stream() {
        Bool newline = new Bool();
        Stream.Builder<String> builder = Stream.builder();

        builder.add(docType.open);
        super.stream().flatMap(s -> {
            switch (s) {
            case NEWLINE:
                if (newline.value) {
                    return Stream.of(NEWLINE, docType.prefix);
                } else {
                    newline.value = true;
                    return Stream.empty();
                }
            default:
                if (newline.value) {
                    newline.value = false;
                    return Stream.of(NEWLINE, docType.prefix, " ", s);
                }
                return Stream.of(s);
            }
        }).forEach(builder::add);
        if (newline.value)
            builder.add(NEWLINE);
        if (!docType.close.isEmpty())
            builder.add(docType.close).add(NEWLINE);
        return builder.build();
    }

    private static class Bool {
        boolean value = false;
    }
}
