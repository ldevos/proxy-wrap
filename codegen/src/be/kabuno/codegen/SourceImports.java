package be.kabuno.codegen;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static be.kabuno.codegen.SourceWriter.NEWLINE;

/**
 * Class that represents the imports in a {@link SourceFile}.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceImports implements SourceIterable {

    private final String activePackage;
    private HashMap<String, String> imports = new HashMap<>(); // qualified -> short

    /**
     * Create a new source imports class.
     * @param activePackage The name of the active class. Classes in the same packages
     *                       will be deemed imported automatically.
     */
    public SourceImports(String activePackage) {
        this.activePackage = activePackage;
    }

    /**
     * Add a new import. If there is a conflict, only the first added
     * import is imported.
     *
     * @param qualifiedName The type to be added, without generic types.
     */
    public void addImport(String qualifiedName) {
        // If we already know this import, do nothing
        if (!imports.containsKey(qualifiedName)) {
            String simple = nameAndGenericTypesOf(qualifiedName);

            // simple name already in use, use qualified name
            if (!simple.equals("*") && imports.values().contains(simple)) {
                imports.put(qualifiedName, qualifiedName);
            }

            // short name not yet in use, import short name
            else {
                imports.put(qualifiedName, simple);
            }
        }
    }

    /**
     * Get the name of the type. Short if it was successfully imported,
     * long otherwise. The given qualified name can have type parameters.
     *
     * @param qualifiedName The type to get the name of, potentially with
     *                      generic parameters.
     */
    public String getType(String qualifiedName) {
        String cleanName = nameWithoutGenericTypes(qualifiedName);
        String pkg = packageOf(qualifiedName);
        String generics = parsedGenerics(genericTypesOf(qualifiedName));

        // java.lang stuff
        if (pkg.equals("java.lang")) {
            return nameAndGenericTypesOf(cleanName) + generics;
        }

        // It's in the active package
        if (pkg.equals(activePackage)) {
            return nameAndGenericTypesOf(cleanName) + generics;
        }

        // Wildcard import
        if (imports.containsKey(pkg + ".*")) {
            return nameAndGenericTypesOf(cleanName) + generics;
        }

        // Specific imports
        return imports.getOrDefault(cleanName, cleanName) + generics;
    }

    private String packageOf(String qualifiedName) {
        int idx1, idx2 = qualifiedName.indexOf('<');
        idx2 = (idx2 == -1) ? qualifiedName.length() : idx2;
        if ((idx1 = qualifiedName.lastIndexOf('.', idx2)) != -1)
            return qualifiedName.substring(0, idx1);
        return "";
    }

    private String nameAndGenericTypesOf(String qualifiedName) {
        return qualifiedName.substring(qualifiedName.lastIndexOf('.') + 1);
    }

    private String nameWithoutGenericTypes(String qualifiedName) {
        int idx;
        if ((idx = qualifiedName.indexOf('<')) != -1)
            return qualifiedName.substring(0, idx);
        return qualifiedName;
    }

    private String[] genericTypesOf(String qualifiedName) {
        int idx1, idx2;
        if ((idx1 = qualifiedName.indexOf('<')) != -1 &&
                (idx2 = qualifiedName.lastIndexOf('>')) != -1)
            return qualifiedName.substring(idx1 + 1, idx2).split("\\s*,\\s*");
        return new String[0];
    }

    private String parsedGenerics(String[] args) {
        if (args.length > 0)
            return "<" + Arrays.stream(args).map(this::getType).collect(Collectors.joining(", ")) + ">";
        return "";
    }

    @Override
    public Stream<String> stream() {
        String[] realImports = imports.entrySet().stream()
                .filter(e -> !e.getKey().equals(e.getValue()))
                .map(Map.Entry::getKey)
                .toArray(String[]::new);

        // Java packages first.
        Arrays.sort(realImports, (o1, o2) -> {
            if (o1.startsWith("java.")) {
                if (o2.startsWith("java."))
                    return o1.compareTo(o2);
                return -1;
            }
            if (o2.startsWith("java."))
                return 1;
            return o1.compareTo(o2);
        });

        return Arrays.stream(realImports)
                .flatMap(s -> Stream.of("import ", s, ";", NEWLINE));
    }
}

