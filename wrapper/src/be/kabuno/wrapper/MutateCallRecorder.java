package be.kabuno.wrapper;

import java.util.ArrayList;

/**
 * A base class for mutator classes. This class stores the {@link MethodCall} objects,
 * and provides helper methods.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class MutateCallRecorder {

    public ArrayList<MethodCall> calls = new ArrayList<>();

    /**
     * Record a method call.
     *
     * @param r A runnable that executes the method. It has all parameters
     *          in its closure.
     */
    public void recordCall(MethodCall r) {
        calls.add(r);
    }

    /**
     * Replay the given method call.
     *
     * @param methodCall The method call to replay.
     * @throws ReplayException An exception thrown by the original method
     *                             wrapped in a ReplayException
     */
    public void replayMethodCall(MethodCall methodCall) throws ReplayException {
        try {
            methodCall.call();
        } catch (Exception e) {
            throw new ReplayException(e);
        }
    }

    /**
     * Replay all recorded method calls.
     *
     * @throws ReplayException An exception thrown by one of the original methods
     *                             wrapped in a ReplayException.
     */
    public void replayAllMethodCalls() throws ReplayException {
        for (MethodCall call : calls) {
            replayMethodCall(call);
        }
    }

    /**
     * A function that, when called, executes a mutate method on the original
     * object.
     */
    @FunctionalInterface
    public interface MethodCall {

        /**
         * Call the original method.
         * @throws Exception An exception thrown by the original method.
         */
        void call() throws Exception;
    }
}
