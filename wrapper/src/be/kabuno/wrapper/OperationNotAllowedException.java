package be.kabuno.wrapper;

public class OperationNotAllowedException extends RuntimeException {
    public OperationNotAllowedException() {
    }

    public OperationNotAllowedException(String message) {
        super(message);
    }
}
