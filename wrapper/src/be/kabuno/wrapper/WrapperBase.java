package be.kabuno.wrapper;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * A base class for generated wrappers.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public abstract class WrapperBase {

    private final Object lock;

    /**
     * Create a new wrapper.
     * @param lock The object to use as lock.
     */
    protected WrapperBase(Object lock) {
        this.lock = Objects.requireNonNull(lock);
    }

    /**
     * Create a new wrapper. Use this object as lock.
     */
    protected WrapperBase() {
        this.lock = this;
    }

    /**
     * Curry the given <tt>wrapFn</tt> such that no lock argument is needed.
     * @param wrapFn The wrap function
     * @param lock The lock
     * @param <OT> The original type
     * @param <WT> The wrap type.
     * @return A wrapper function that does not need a lock argument.
     */
    public static <OT, WT> Function<OT, WT> curryWrapFn(BiFunction<OT, Object, WT> wrapFn, Object lock) {
        return (OT orig) -> wrapFn.apply(orig, lock);
    }

    /**
     * Delegate to the static {@link #_unwrap(Object, Object)} and pass this wrapper's
     * lock.
     * <p> Not type safe.</p>
     *
     * @see #_wrap(Object, Object, Class)
     */
    protected <OT, WT> OT _unwrap(WT wrapped) {
        return _unwrap(wrapped, lock);
    }

    /**
     * Look through the given wrapped object's class hierarchy for the
     * right unwrap method.
     * <p> <b>This method is not type safe.</b> Use dedicated methods instead.</p>
     *
     * @param wrapped The wrapped object
     * @param lock The wrap lock
     * @param <OT> The original type
     * @param <WT> The wrapped type
     * @return The original object, or null, if the given wrapped object was null.
     * @throws RuntimeException The given object is not a wrapped object.
     * @throws ClassCastException Types don't match
     */
    @SuppressWarnings("unchecked")
    public static <OT, WT> OT _unwrap(WT wrapped, Object lock) {
        if (wrapped == null)
            return null;

        // a generated wrapper implementation is always an Impl
        // class inside an interface
        Class<?> interfaze = wrapped.getClass().getDeclaringClass();
        if (interfaze == null)
            throw new RuntimeException("not a wrapped object");

        // get its unwrap method
        try {
            Method unwrap = interfaze.getMethod("unwrap", interfaze, Object.class);
            Object orig = unwrap.invoke(interfaze, wrapped, lock);
            return (OT) orig;
        } catch (NoSuchMethodException | IllegalAccessException e) {
            throw new RuntimeException("not a wrapped object", e);
        } catch (InvocationTargetException e) {
            if (e.getCause() == null)
                throw new RuntimeException("unexpected error during unwrapping");
            throw new RuntimeException(e.getCause().getMessage(), e.getCause());
        }
    }

    /**
     * Delegates to the static {@link #_wrap(Object, Object, Class) _wrap} and passes this
     * wrapper's lock object.
     * <p> Not type safe, use specific methods.</p>
     *
     * @see #_wrap(Object, Object, Class)
     */
    protected <OT, WT> WT _wrap(OT original, Class<?> baseType) {
        return _wrap(original, lock, baseType);
    }

    /**
     * Wrap the given original object. Look through the class hierarchy to find
     * the most specific {@link WrapType}.
     * <p> This is not type-safe. Use a helper method.</p>
     *
     * @param original The original object to wrap.
     * @param lock The wrap lock
     * @param baseType An upper bound on the type of the original.
     * @param <OT> The original type
     * @param <WT> The wrapped type
     * @return The wrapped original
     * @throws RuntimeException wrong types
     */
    @SuppressWarnings("unchecked")
    public static <OT, WT> WT _wrap(OT original, Object lock, Class<?> baseType) {
        if (original == null)
            return null;

        Class<?> type = findWrapType(baseType, original.getClass());

        // Get its Impl inner class
        try {
            String wrapperName = type.getAnnotation(WrapType.class).accessor();
            Class<?> wrappedType = baseType.getClassLoader().loadClass(wrapperName + "$Impl");
            Constructor<?> constructor = wrappedType.getConstructor(type, Object.class);
            Object wrapped = constructor.newInstance(original, lock);
            return (WT) wrapped;
        } catch (Exception e) {
            throw new RuntimeException("wrapping failed", e);
        }
    }

    private static HashMap<Integer, Class<?>> wrapTypeClassCache = new HashMap<>();

    private static Class<?> findWrapType(Class<?> base, Class<?> orig) {
        // Look in cache
        int key = (97 + base.hashCode()) * 31 + orig.hashCode();
        if (wrapTypeClassCache.containsKey(key)) {
            //System.out.println("cache hit for " + base + ", " + orig + " => " + wrapTypeClassCache.get(key));
            return wrapTypeClassCache.get(key);
        }

        Class<?> result = null;

        WrapType anno = orig.getAnnotation(WrapType.class);
        if (anno != null && !anno.accessor().isEmpty()) {
            result = orig;
        } else {
            Class<?> wrapType = typeSearch(base, orig, null);
            if (wrapType != null)
                result = wrapType;
        }

        // Update cache
        if (result != null) {
            wrapTypeClassCache.put(key, result);
            return result;
        }

        throw new RuntimeException("wrapping failed, no wrap-type found");
    }

    /**
     * We want to look through the subtypes of the given specific type
     * until we find the base type. Then we an to find the wrap type that
     * is closest to the specific type.
     *
     * @return The most specific wrap-type on the path from the goal to
     *          the specific type, or null if no wrap-type was found on
     *          the path.
     */
    static Class<?> typeSearch(Class<?> base, Class<?> specific, Class<?> wrapType) {

        // We haven't yet passed a WrapType-annotated type on our path
        if (wrapType == null) {
            WrapType anno = specific.getAnnotation(WrapType.class);
            if (anno != null && !anno.accessor().isEmpty())
                wrapType = specific;
        }

        if (base.equals(specific))
            return wrapType;

        Class<?> result;

        // Loop over all sub-types of the specific type (depth first search)
        for (Class<?> aClass : getSuperTypes(specific)) {
            //System.out.println(specific + " -> " + aClass + " (wrapType=" + wrapType + ", base=" + base + ")");
            result = typeSearch(base, aClass, wrapType);

            // We found a path from the specific class to base class with a wrap-type class somewhere
            // on that path! return it
            if (result != null)
                return result;
        }

        return null;
    }

    private static Class<?>[] getSuperTypes(Class<?> clazz) {
        Class<?> superType = clazz.getSuperclass();
        if (superType != null) {
            Class<?>[] interfaces = clazz.getInterfaces();
            Class[] arr = new Class[interfaces.length + 1];
            arr[0] = superType;
            System.arraycopy(interfaces, 0, arr, 1, interfaces.length);
            return arr;
        } else {
            return clazz.getInterfaces();
        }
    }
}
