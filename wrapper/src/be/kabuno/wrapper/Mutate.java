package be.kabuno.wrapper;

import java.lang.annotation.*;

/**
 * Use this annotation on a method to indicate that it is publicly available on
 * wrapped mutators.
 *
 * TODO link to wrapped mutator.
 *
 * @author Laurens Devos
 * @since 1.0
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
@Documented
public @interface Mutate {}
