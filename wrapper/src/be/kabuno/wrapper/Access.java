package be.kabuno.wrapper;

import java.lang.annotation.*;

/**
 * Use this annotation to indicate that a method can be used from the outside
 * world. Methods annotated with this annotation are considered part of the
 * public API.
 * <p> Usually, such methods do not modify the state of the object, or if they
 * do, the change is safe (i.e. it does not break any invariant).</p>
 *
 * TODO add wrap_as property
 *
 * @author Laurens Devos
 * @since 1.0
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
@Documented
public @interface Access {}
