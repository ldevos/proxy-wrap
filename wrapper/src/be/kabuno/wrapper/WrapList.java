package be.kabuno.wrapper;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * A wrap list is an immutable list, very much like the ones returned by
 * {@link java.util.Collections#unmodifiableList(List)}.
 *
 * @param <WT> Type of the elements stored by this list.
 * @author Laurens Devos
 * @since 1.0
 */
public final class WrapList<OT, WT> implements List<WT> {

    private final List<? extends OT> original;
    private final Function<OT, WT> wrapper;

    /**
     * Create a new wrap list.
     * @param original The original list
     * @param wrapper A reference to a function that can wrap elements of this list. This
     *                function must have a lock in its scope.
     */
    public WrapList(List<? extends OT> original, Function<OT, WT> wrapper) {
        this.original = original;
        this.wrapper = wrapper;
    }

    @Override
    public WT get(int index) {
        return wrapper.apply(original.get(index));
    }

    @Override
    public WT set(int index, WT element) {
        throw new OperationNotAllowedException();
    }

    @Override
    public void add(int index, WT element) {
        throw new OperationNotAllowedException();
    }

    @Override
    public WT remove(int index) {
        throw new OperationNotAllowedException();
    }

    @Override
    public int indexOf(Object o) {
        return original.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return original.lastIndexOf(o);
    }

    @Override
    public ListIterator<WT> listIterator() {
        return new LstItr();
    }

    @Override
    public ListIterator<WT> listIterator(int index) {
        return new LstItr(index);
    }

    @Override
    public WrapList<OT, WT> subList(int fromIndex, int toIndex) {
        return new WrapList<OT, WT>(original.subList(fromIndex, toIndex), wrapper);
    }

    @Override
    public int size() {
        return original.size();
    }

    @Override
    public boolean isEmpty() {
        return original.isEmpty();
    }

    /**
     * @throws OperationNotAllowedException the given object cannot be unwrapped.
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean contains(Object o) {
        // TODO fix
        throw new OperationNotAllowedException();
        // if (clazz.isInstance(o)) {
        //     OT orig = unwrapper.wrap((WT) o, lock);
        //     return original.contains(orig);
        // }
        // return original.contains(o);
    }

    @Override
    public Iterator<WT> iterator() {
        return new Itr();
    }

    @Override
    public Object[] toArray() {
        return original.stream().map(wrapper).toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(WT e) {
        throw new OperationNotAllowedException();
    }

    @Override
    public boolean remove(Object o) {
        throw new OperationNotAllowedException();
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends WT> c) {
        throw new OperationNotAllowedException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends WT> c) {
        throw new OperationNotAllowedException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new OperationNotAllowedException();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new OperationNotAllowedException();
    }

    @Override
    public void clear() {
        throw new OperationNotAllowedException();
    }

    private class Itr implements Iterator<WT> {
        final Iterator<? extends OT> it = original.iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public WT next() {
            return wrapper.apply(it.next());
        }

        @Override
        public void remove() {
            throw new OperationNotAllowedException();
        }
    }

    private class LstItr implements ListIterator<WT> {

        final ListIterator<? extends OT> it;

        LstItr() {
            it = original.listIterator();
        }

        LstItr(int idx) {
            it = original.listIterator(idx);
        }

        @Override
        public boolean hasPrevious() {
            return it.hasPrevious();
        }

        @Override
        public WT previous() {
            return wrapper.apply(it.previous());
        }

        @Override
        public int nextIndex() {
            return it.nextIndex();
        }

        @Override
        public int previousIndex() {
            return it.previousIndex();
        }

        @Override
        public void remove() {
            throw new OperationNotAllowedException();
        }

        @Override
        public void set(WT e) {
            throw new OperationNotAllowedException();
        }

        @Override
        public void add(WT e) {
            throw new OperationNotAllowedException();
        }

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public WT next() {
            return wrapper.apply(it.next());
        }
    }

    /** A random access version of this list. Exactly the same, but with
     * the marker interface. */
    //interface RandomAccessWrapList extends WrapList, RandomAccess {}
}
