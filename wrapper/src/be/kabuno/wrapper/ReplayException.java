package be.kabuno.wrapper;

/**
 * Exception that wraps an exception thrown by an original mutate method.
 * @author Laurens Devos
 * @since 1.0
 */
public class ReplayException extends Exception {
    public ReplayException(Throwable cause) {
        super(cause);
    }
}
