package be.kabuno.wrapper;

import java.lang.annotation.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Use this annotation on types that need to be processed by the annotation processor.
 *
 * @author Laurens Devos
 * @since 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WrapType {

    /**
     * Set this property if you want to generate an access wrapper.
     * This must be a fully qualified name.
     */
    String accessor() default "";

    /**
     * Set this property if you want to generate a mutator.
     * This must be a fully qualified name.
     */
    String mutator() default "";

    /**
     * Set this property if you want a wrap and unwrap helper
     * method generated in the given wrapper.
     * This must be a fully qualified name.
     */
    String wrapper() default "";


    // Property names

    /** Name of accessor property. */
    String ACCESSOR = "accessor";

    /** Name of mutator property. */
    String MUTATOR = "mutator";

    /** Name of wrapper property. */
    String WRAPPER = "wrapper";
}
