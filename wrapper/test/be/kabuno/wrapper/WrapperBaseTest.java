package be.kabuno.wrapper;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test for {@link WrapperBase}.
 */
public class WrapperBaseTest {

    static Object lock = new Object();

    @Test
    public void typeSearch1() throws Exception {
        Class<?> res;

        System.out.println("--- Car4 -> Object");
        res = WrapperBase.typeSearch(Object.class, Car4.class, null);
        assertEquals(Car3.class, res);

        System.out.println("--- Car4 -> Car0");
        res = WrapperBase.typeSearch(Car0.class, Car4.class, null);
        assertEquals(Car3.class, res);

        System.out.println("--- Car3 -> Car3");
        res = WrapperBase.typeSearch(Car3.class, Car3.class, null);
        assertEquals(Car3.class, res);

        System.out.println("--- Car3 -> Car2");
        res = WrapperBase.typeSearch(Car2.class, Car3.class, null);
        assertEquals(Car3.class, res);

        System.out.println("--- Car2 -> Car1");
        res = WrapperBase.typeSearch(Car1.class, Car2.class, null);
        assertEquals(Car2.class, res);
    }

    @Test
    public void typeSearch2() throws Exception {
        Class<?> res;

        System.out.println("--- Wagon2 -> IWagon1");
        res = WrapperBase.typeSearch(IWagon1.class, Wagon2.class, null);
        assertEquals(IWagon2.class, res);
    }

    @Test
    public void wrap1() throws Exception {
        System.out.println("--- Wrap Car4 -> Car2Access");
        Car2Access w1 = WrapperBase._wrap(new Car4(), lock, Car1.class);
        assertEquals(Car3Access.class, w1.getClass());

        System.out.println("--- Wrap Car4 -> Car2Access");
        Car2Access w2 = WrapperBase._wrap((Car0) new Car4(), lock, Car0.class);
        assertEquals(Car3Access.class, w2.getClass());
    }

    @Test
    public void wrap2() throws Exception {
        System.out.println("--- Wrap ProjectImpl -> ProjectAccess");
        ProjectImpl pi = new ProjectImpl();
        ProjectAccess pa = WrapperBase._wrap(pi, lock, Subsystem3.class);
        assertEquals(ProjectAccess.class, pa.getClass());
    }

    @Test
    public void wrap3() throws Exception {
        System.out.println("--- Wrap ProjectImpl -> HasComments");
        ProjectImpl pi = new ProjectImpl();
        HasCommentsAccess hca = WrapperBase._wrap(pi, lock, HasComments.class);
        assertEquals(ProjectAccess.class, hca.getClass());
    }

    @Test
    public void unwrap1() throws Exception {

    }
}

class Car0 {}
class Car1 extends Car0 {}
@WrapType(accessor = "be.kabuno.wrapper.Car2Access")
class Car2 extends Car1 {}
@WrapType(accessor = "be.kabuno.wrapper.Car3Access")
class Car3 extends Car2 {}
class Car4 extends Car3 {}

interface Car2Access {
    static Car2Access wrap(Car2 original, Object lock) { return new Car2Access() {}; }
}

interface Car3Access extends Car2Access {
    static Car3Access wrap(Car3 original, Object lock) {
        return new Car3Access() {};
    }
    static Car3 unwrap(Car3Access wrapped, Object lock) {
        return new Car3();
    }
}

class Wagon0 implements IWagon1 {}
class Wagon1 extends Wagon0 {}
class Wagon2 extends Wagon1 implements IWagon3 {}

interface IWagon1 {}
@WrapType(accessor = "not empty")
interface IWagon2 extends IWagon1 {}
interface IWagon3 extends IWagon2 {}


// -----
class ProjectImpl extends Project {}
@WrapType(accessor = "be.kabuno.wrapper.ProjectAccess")
class Project extends Subsystem1 {}
class ProjectAccess extends SubsystemAccess implements HasCommentsAccess {
    public static ProjectAccess wrap(Project original, Object lock) {
        return new ProjectAccess();
    }
}


@WrapType(accessor = "be.kabuno.wrapper.Subsystem")
class Subsystem extends Subsystem3 implements HasComments {}

class SubsystemAccess {
    public static SubsystemAccess wrap(Subsystem original, Object lock) {
        return new SubsystemAccess();
    }
}

class Subsystem1 extends Subsystem2 {}
class Subsystem2 extends Subsystem {}
class Subsystem3 {}

@WrapType(accessor = "be.kabuno.wrapper.HasCommentsAccess")
interface HasComments {}

interface HasCommentsAccess {
    static HasCommentsAccess wrap(HasComments original, Object lock) {
        return new HasCommentsAccess() {
        };
    }
}
