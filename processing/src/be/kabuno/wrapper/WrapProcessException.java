package be.kabuno.wrapper;

import javax.lang.model.element.Element;
import javax.tools.Diagnostic;
import java.util.Objects;

/**
 * An exception that is turned into an annotation processing
 * {@link javax.annotation.processing.Messager#printMessage(Diagnostic.Kind, CharSequence) error}.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class WrapProcessException extends Exception {
    private final Element element;

    public WrapProcessException(Element e, String message) {
        super(message);
        element = Objects.requireNonNull(e);
    }

    public WrapProcessException(Element e, Throwable cause, String message) {
        super(message, cause);
        element = Objects.requireNonNull(e);
    }

    public Element getElement() {
        return element;
    }
}
