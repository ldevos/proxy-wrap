package be.kabuno.wrapper;

import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import java.util.stream.Stream;

/**
 * An inner wrap-type class of an {@link SourceTypeInfo}.
 * An inner source type must be a concrete inner class of a source type
 * and it must also implement/extend its source type.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceInnerTypeInfo {

    private final ProcessContext ctx;
    private final SourceTypeInfo enclosing;
    private final TypeElement typeElement;

    public SourceInnerTypeInfo(ProcessContext ctx, SourceTypeInfo enclosing, TypeElement typeElement) {
        this.ctx = ctx;
        this.enclosing = enclosing;
        this.typeElement = typeElement;
    }

    /**
     * Get the methods annotated with {@link Access} that
     * aren't already in the enclosing type.
     *
     * You can't mark a method overridden method @Access if it
     * wasn't @Access in the source type.
     */
    public Stream<MethodInfo> getMethods() {
        return typeElement.getEnclosedElements().stream()
                .filter(m -> m.getKind().equals(ElementKind.METHOD))
                .map(ExecutableElement.class::cast)
                .filter(m -> m.getAnnotation(Access.class) != null)
                .map(m -> new MethodInfo(ctx, m))
                .filter(m -> !enclosing.getMethodSet().contains(m));
    }

    /**
     * Get the name of this inner class.
     * @return The name of this class.
     */
    public String getName() {
        return typeElement.getSimpleName().toString();
    }
}
