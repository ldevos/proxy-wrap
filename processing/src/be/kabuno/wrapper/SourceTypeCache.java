package be.kabuno.wrapper;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import java.util.HashMap;

/**
 * A cache for {@link SourceTypeInfo}. This class is responsible for the creation of
 * {@link SourceTypeInfo source type infos}.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceTypeCache {

    private final ProcessContext ctx;
    private final HashMap<String, SourceTypeInfo> cache;

    /**
     * Create a new cache.
     * @param ctx The processing context.
     */
    public SourceTypeCache(ProcessContext ctx) {
        this.ctx = ctx;
        this.cache = new HashMap<>();
    }

    /**
     * Get a source type out of the cache.
     * @param element The element
     * @return A source type info object for the given element.
     * @throws WrapProcessException The given element isn't a valid source type.
     */
    public SourceTypeInfo get(TypeElement element) throws WrapProcessException {
        String key = element.getQualifiedName().toString();

        if (cache.containsKey(key))
            return cache.get(key);

        // Create a new source type info for this element
        SourceTypeInfo sourceType = new SourceTypeInfo(ctx, this, element);
        cache.put(key, sourceType);
        return sourceType;
    }

    /**
     * Get a source type out of the cache.
     * @param element The element you
     * @return A source type info instance
     * @throws WrapProcessException the given element isn't a valid source type
     */
    public SourceTypeInfo get(Element element) throws WrapProcessException {
        if (!element.asType().getKind().equals(TypeKind.DECLARED))
            throw new WrapProcessException(element, "invalid source type: not a declared type");
        return get((TypeElement) element);
    }
}
