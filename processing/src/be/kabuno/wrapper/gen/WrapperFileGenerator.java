package be.kabuno.wrapper.gen;

import be.kabuno.codegen.SourceFile;
import be.kabuno.codegen.SourceMethod;
import be.kabuno.codegen.SourceWriter;
import be.kabuno.wrapper.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import static be.kabuno.codegen.SourceWriter.NEWLINE;
import static be.kabuno.codegen.SourceWriter.TYPE;

/**
 * A file generator for wrapper files.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class WrapperFileGenerator extends FileGenerator {

    private final SourceFile file;

    /**
     * Create a new file generator.
     *
     * @param outputType The details of the outputted type.
     * @param cache      A cache for source type infos
     */
    public WrapperFileGenerator(OutputType outputType, SourceTypeCache cache) {
        super(outputType);
        this.file = new SourceFile(outputType.getPackage(), outputType.getSimpleName());

        initialize();
    }

    private void initialize() {
        file.type.setModifiers("public final class");
        file.type.extendsClause.put("extends ", TYPE, WrapperBase.class.getCanonicalName());

        file.imports.addImport("be.kabuno.wrapper.*");

        file.type.docs.putln(NEWLINE, "This is a wrapper for:");

        SourceMethod constr1 = new SourceMethod(file.type.name);
        constr1.setModifiers("public");
        constr1.addParam("java.lang.Object", "lock");
        constr1.body.putln("super(lock);");
        file.type.addMember(constr1);

        SourceMethod constr2 = new SourceMethod(file.type.name);
        constr2.setModifiers("public");
        constr2.body.putln("super();");
        file.type.addMember(constr2);
    }

    /**
     * Add a helper methods to this wrapper file.
     * @param sourceType The source file to add a helper for.
     */
    public void addHelper(SourceTypeInfo sourceType) {
        if (!sourceType.hasProperty(WrapType.ACCESSOR))
            throw new IllegalArgumentException("no accessor");
        if (!outputType.equals(sourceType.getProperty(WrapType.WRAPPER)))
            throw new IllegalArgumentException("shouldn't be in this wrapper");

        file.imports.addImport(sourceType.getPackage() + ".*");
        file.type.docs.putln(" - {@link ", TYPE, sourceType.getQualifiedName(), "}");

        OutputType export = sourceType.getProperty(WrapType.ACCESSOR);

        SourceMethod wrap = new SourceMethod("wrap");
        wrap.setModifiers("public");
        wrap.addParam(sourceType.getCodeName(), "original");
        wrap.setReturnType(export.getQualifiedName());
        wrap.body.putln("return super._wrap(original, ", TYPE, sourceType.getQualifiedName(), ".class);");
        file.type.addMember(wrap);

        SourceMethod unwrap = new SourceMethod("unwrap");
        unwrap.setModifiers("public");
        unwrap.addParam(export.getQualifiedName(), "wrapped");
        unwrap.setReturnType(sourceType.getCodeName());
        unwrap.body.putln("return super._unwrap(wrapped);");
        file.type.addMember(unwrap);

        // Does not work because of type erasure
        // SourceMethod wrapList = new SourceMethod("wrapList");
        // wrapList.setModifiers("public");
        // wrapList.returnType.put(TYPE, WrapList.class.getCanonicalName(),
        //         "<", TYPE, sourceType.getCodeName(), ", ", TYPE, export.getCodeName(), "> ");
        // wrapList.addParamRaw("original", TYPE, List.class.getCanonicalName(), "<? extends ", TYPE, sourceType.getCodeName(), ">");
        // wrapList.body.putln("return new ", TYPE, WrapList.class.getCanonicalName(), "<>(original, this::wrap);");
        // file.type.addMember(wrapList);
    }

    @Override
    public void writeFile(BufferedWriter writer) throws IOException {
        SourceWriter.write(writer, file);
    }
}
