package be.kabuno.wrapper.gen;

import be.kabuno.codegen.*;
import be.kabuno.wrapper.*;
import be.kabuno.wrapper.transf.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Optional;

import static be.kabuno.codegen.SourceWriter.NEWLINE;
import static be.kabuno.codegen.SourceWriter.TYPE;

/**
 * A file generator for access files.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class AccessFileGenerator extends FileGenerator {

    private final TypeTransformer returnTransformer;
    private final TypeTransformer paramTransformer;

    private final SourceTypeInfo source;
    private final SourceFile file;
    private final SourceType impl;

    /** The output type of the wrap type super class, null if not present */
    private final OutputType superClassOutput;

    /**
     * Create a new file generator.
     *
     * @param ctx        The processing context
     * @param sourceType The type this generated type is based on.
     * @param outputType The details of the outputted type.
     * @param cache      A cache for source type infos
     */
    public AccessFileGenerator(ProcessContext ctx, SourceTypeInfo sourceType, OutputType outputType, SourceTypeCache cache)
            throws WrapProcessException {
        super(outputType);

        source = sourceType;
        file = new SourceFile(outputType.getPackage(), outputType.getSimpleName());
        impl = new SourceType("Impl");

        returnTransformer = new ReturnTypeTransf(ctx, cache);
        paramTransformer = new ParamTypeTransf(ctx, cache);

        // Initialize super class
        Optional<SourceTypeInfo> superOpt = source.getWrapTypeSuperClass()
                .filter(s -> s.hasProperty(WrapType.ACCESSOR));
        if (superOpt.isPresent()) {
            if (superOpt.get().getKind().equals(SourceTypeInfo.Kind.INTERFACE))
                throw new AssertionError("should not be an interface - fix it");
            superClassOutput = superOpt.get().getProperty(WrapType.ACCESSOR);
        } else {
            superClassOutput = null;
        }

        // Docs
        file.type.docs.putln(NEWLINE, "This is the wrapped access type for {@link ", TYPE, sourceType.getCodeName(),
                "}.", NEWLINE, "This file was automatically generated, do not modify.");

        // Import for everything in original package
        file.imports.addImport("be.kabuno.wrapper.*");
        if (!outputType.getPackage().equals(sourceType.getPackage()))
             file.imports.addImport(sourceType.getPackage() + ".*");

        // Write class
        initializeInterface();
        initializeImpl();
        addWrapMethods();
    }

    private void initializeInterface() throws WrapProcessException {
        file.type.setModifiers("public interface");

        // if the source type is a class, check for super class
        if (superClassOutput != null)
            file.type.extendsClause.put("extends ", TYPE, superClassOutput.getQualifiedName());

        // source-type super interfaces
        source.getWrapTypeInterfaces()
                .filter(s -> s.hasProperty(WrapType.ACCESSOR))
                .forEach(s -> {
                    OutputType out = s.getProperty(WrapType.ACCESSOR);
                    if (file.type.extendsClause.isEmpty()) {
                        file.type.extendsClause.put("extends ", TYPE, out.getQualifiedName());
                    } else {
                        file.type.extendsClause.put(", ", TYPE, out.getQualifiedName());
                    }
                });
    }

    private void initializeImpl() throws WrapProcessException {
        impl.setModifiers("class");
        setImplFieldsAndConstructor(impl, superClassOutput != null);

        // extend super class
        if (superClassOutput != null) {
            impl.extendsClause.put("extends ", TYPE, superClassOutput.getQualifiedName(), ".Impl ");
        }

        // implement this interface
        impl.extendsClause.put("implements ", TYPE, outputType.getQualifiedName());

        // source-type super interfaces
        source.getWrapTypeInterfaces()
                .filter(s -> s.hasProperty(WrapType.ACCESSOR))
                .forEach(s -> {
                    OutputType out = s.getProperty(WrapType.ACCESSOR);
                    impl.extendsClause.put(", ", TYPE, out.getQualifiedName());
                });

    }

    private void addWrapMethods() {
        SourceMethod wrap = new SourceMethod("wrap");
        wrap.setModifiers("public static");
        wrap.setReturnType(outputType.getQualifiedName());
        wrap.addParam(source.getQualifiedName(), "original");
        wrap.addParam("java.lang.Object", "lock");
        wrap.body.putln("return ", TYPE, WrapperBase.class.getCanonicalName(),
                ".<", TYPE, source.getCodeName(), ", ", TYPE, outputType.getCodeName(),
                ">_wrap(original, lock, ", TYPE, source.getQualifiedName(), ".class);");
        file.type.addMember(wrap);

        SourceMethod unwrap = new SourceMethod("unwrap");
        unwrap.setModifiers("public static");
        //unwrap.addThrowType(OperationNotAllowedException.class);
        unwrap.setReturnType(source.getQualifiedName());
        unwrap.addParam(outputType.getQualifiedName(), "wrapped");
        unwrap.addParam("java.lang.Object", "lock");
        unwrap.body.openBlock("if (wrapped instanceof Impl)");
        unwrap.body.putln("Impl impl = (Impl) wrapped;");
        unwrap.body.openBlock("if (impl.lock != lock)");
        unwrap.body.putln("throw new ", TYPE, OperationNotAllowedException.class.getCanonicalName(),
                "(\"cannot unwrap object, different lock\");");
        unwrap.body.closeBlock();
        unwrap.body.putln("return impl.original;");
        unwrap.body.closeBlock();
        unwrap.body.putln("return ", TYPE, WrapperBase.class.getCanonicalName(), "._unwrap(wrapped, lock);");
        file.type.addMember(unwrap);

    }

    private void setImplFieldsAndConstructor(SourceType type, boolean callSuper) {
        type.addField("private final", source.getQualifiedName(), "original");
        type.addField("private final", "java.lang.Object", "lock");
        SourceMethod constr = new SourceMethod(type.name);
        constr.setModifiers("public");
        constr.addParam(source.getQualifiedName(), "original");
        constr.addParam("java.lang.Object", "lock");
        if (callSuper)
            constr.body.putln("super(original, lock);");
        constr.body.putln("this.original = original;");
        constr.body.putln("this.lock = lock;");
        type.addMember(constr);
    }

    public void addMethod(MethodInfo method) throws WrapProcessException {
        if (!method.hasAnnotation(Access.class)) // ignore
            return;
        if (!isValidMethod(method))
            throw new WrapProcessException(method.asElement(), "Access method must be public.");

        TransformedMethod tm = transformMethod(method);

        // Add method to interface
        SourceMethod intrMethod = new SourceMethod(method.getName());
        setMethodParameters(tm, intrMethod);
        intrMethod.setReturnType(tm.getTransformedReturnType().getTransf());

        // Copy docs
        if (method.getDocs() != null) {
            String[] docs = method.getDocs().trim().split("\n ?");
            intrMethod.docs.putln();
            for (String doc : docs)
                intrMethod.docs.putln(doc);
        }


        // Add method to implementation
        SourceMethod implMethod = new SourceMethod(method.getName());
        setImplMethod(tm, implMethod);

        file.type.addMember(intrMethod);
        impl.addMember(implMethod);
    }

    private void setImplMethod(TransformedMethod tm, SourceMethod m) throws WrapProcessException {
        setMethodParameters(tm, m);
        m.setReturnType(tm.getTransformedReturnType().getTransf());
        m.setModifiers("public");
        m.body.put("return ");
        m.body.put(tm.getTransformedReturnType().convert(tm.getMethodCall()));
        m.body.putln(";");
    }

    private void setMethodParameters(TransformedMethod method, SourceMethod source) throws WrapProcessException {
        method.getTransformedParams().stream().forEach(p -> source.addParam(p.type.getTransf(), p.name));
    }

    public TransformedMethod transformMethod(MethodInfo method) {
        return new TransformedMethod(method, returnTransformer, paramTransformer);
    }

    private void addInnerClass(SourceInnerTypeInfo innerType) throws WrapProcessException {
        SourceType inner = new SourceType(innerType.getName());
        inner.setModifiers("class");
        inner.extendsClause.put("extends Impl");
        setImplFieldsAndConstructor(inner, true);

        Iterable<MethodInfo> it = innerType.getMethods()::iterator;
        for (MethodInfo method : it) {
            if (!isValidMethod(method))
                throw new WrapProcessException(method.asElement(),
                        "invalid inner class method '" + method.getName() + "': must be public");
            TransformedMethod tm = transformMethod(method);
            SourceMethod m = new SourceMethod(method.getName());
            setImplMethod(tm, m);
            inner.addMember(m);
        }

        file.type.addMember(inner);
    }

    private boolean isValidMethod(MethodInfo method) {
        return method.isPublic();
    }

    @Override
    public void writeFile(BufferedWriter writer) throws IOException, WrapProcessException {
        SourceBuffer spacer = new SourceBuffer();
        spacer.putln(NEWLINE, NEWLINE, "// -- implementation --", NEWLINE);
        file.type.addMember(spacer);
        file.type.addMember(impl);

        // Iterable<SourceInnerTypeInfo> it = source.getInnerClasses()::iterator;
        // for (SourceInnerTypeInfo innerType : it) {
        //     addInnerClass(innerType);
        // }

        SourceWriter.write(writer, file);
    }
}
