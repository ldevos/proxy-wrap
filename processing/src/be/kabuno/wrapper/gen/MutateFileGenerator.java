package be.kabuno.wrapper.gen;

import be.kabuno.codegen.SourceFile;
import be.kabuno.codegen.SourceMethod;
import be.kabuno.codegen.SourceWriter;
import be.kabuno.wrapper.*;
import be.kabuno.wrapper.transf.ParamTypeTransf;
import be.kabuno.wrapper.transf.TransformedMethod;
import be.kabuno.wrapper.transf.TransformedType;
import be.kabuno.wrapper.transf.TypeTransformer;

import javax.lang.model.type.TypeKind;
import java.io.BufferedWriter;
import java.io.IOException;

import static be.kabuno.codegen.SourceWriter.*;

/**
 * A file generator for mutate files.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class MutateFileGenerator extends FileGenerator {

    private final TypeTransformer paramTransformer;

    private final SourceFile file;
    private final SourceTypeInfo source;

    /**
     * Create a new file generator.
     *
     * @param ctx        The processing context
     * @param source     The type this generated type is based on.
     * @param outputType The details of the outputted type.
     * @param cache      A cache for source type infos
     */
    public MutateFileGenerator(ProcessContext ctx, SourceTypeInfo source, OutputType outputType, SourceTypeCache cache) {
        super(outputType);

        this.source = source;
        this.file = new SourceFile(outputType.getPackage(), outputType.getSimpleName());

        this.paramTransformer = new ParamTypeTransf(ctx, cache);

        initialize();
        addMutateMethods();
    }

    private void initialize() {
        file.type.setModifiers("public final class");

        file.imports.addImport("be.kabuno.wrapper.*");

        file.type.addField("private final", MutateCallRecorder.class.getCanonicalName(), "recorder");
        file.type.addField("private final", source.getQualifiedName(), "original");
        file.type.addField("private final", "java.lang.Object", "lock");
        SourceMethod constr = new SourceMethod(outputType.getSimpleName());
        constr.setModifiers("public");
        constr.addParam(source.getQualifiedName(), "original");
        constr.addParam("java.lang.Object", "lock");
        constr.body.putln("this.recorder = new ", TYPE, MutateCallRecorder.class.getCanonicalName(), "();");
        constr.body.putln("this.original = original;");
        constr.body.putln("this.lock = lock;");
        file.type.addMember(constr);
    }

    private void addMutateMethods() {
        SourceMethod replay = new SourceMethod("replay");
        replay.setModifiers("public static");
        replay.addParam(outputType.getQualifiedName(), "mutator");
        replay.addParam("java.lang.Object", "lock");
        replay.addThrowType(ReplayException.class.getCanonicalName());
        replay.setReturnType("void");
        replay.body.openBlock("if (mutator.lock != lock)");
        replay.body.putln("throw new OperationNotAllowedException(\"cannot replay method calls: different locks\");");
        replay.body.closeBlock();
        replay.body.putln("mutator.recorder.replayAllMethodCalls();");
        file.type.addMember(replay);
    }

    public void addMethod(MethodInfo method) throws WrapProcessException {
        if (!method.hasAnnotation(Mutate.class)) // ignore
            return;
        if (!isValidMethod(method))
            throw new WrapProcessException(method.asElement(), "mutate method must be public and return void");

        TransformedMethod tt = new TransformedMethod(method, subject -> type -> new TransformedType(type.toString()),
                paramTransformer);

        SourceMethod m = new SourceMethod(method.getName());
        m.setReturnType("void");
        m.setModifiers("public");
        tt.getTransformedParams().forEach(p -> m.addParam(p.type.getTransf(), p.name));
        m.body.putln("recorder.recordCall(() -> {", INDENT);
        m.body.put(tt.getMethodCall());
        m.body.putln(";", NEWLINE, UN_INDENT, "});");
        file.type.addMember(m);
    }

    private boolean isValidMethod(MethodInfo method) {
        return method.isPublic() && method.getReturnType().getTypeMirror().getKind().equals(TypeKind.VOID);
    }

    @Override
    public void writeFile(BufferedWriter writer) throws IOException {
        SourceWriter.write(writer, file);
    }
}
