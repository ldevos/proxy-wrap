package be.kabuno.wrapper.gen;

import be.kabuno.wrapper.OutputType;
import be.kabuno.wrapper.WrapProcessException;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Base class for file generators.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public abstract class FileGenerator {

    /** The information about the generated type. */
    protected final OutputType outputType;

    /**
     * Create a new file generator.
     *
     * @param outputType The details of the outputted type.
     */
    protected FileGenerator(OutputType outputType) {
        this.outputType = outputType;
    }

    /**
     * Write this generated type to a file.
     * @param writer The writer to use.
     * @throws IOException Thrown by the writer
     */
    public abstract void writeFile(BufferedWriter writer) throws IOException, WrapProcessException;

    /**
     * Get the associated output file.
     * @return The output file associated with this file generator.
     */
    public OutputType getOutputType() {
        return outputType;
    }
}
