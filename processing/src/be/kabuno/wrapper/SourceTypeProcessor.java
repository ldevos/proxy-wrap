package be.kabuno.wrapper;

import be.kabuno.wrapper.gen.AccessFileGenerator;
import be.kabuno.wrapper.gen.MutateFileGenerator;
import be.kabuno.wrapper.gen.WrapperFileGenerator;

import javax.tools.JavaFileObject;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;

/**
 * A processor of {@link SourceTypeInfo} objects (i.e. valid classes annotated with {@link WrapType}.)
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceTypeProcessor {

    private final ProcessContext ctx;
    private final SourceTypeCache cache;
    private final HashMap<OutputType, WrapperFileGenerator> wrapperFiles;

    public SourceTypeProcessor(ProcessContext ctx, SourceTypeCache cache) {
        this.ctx = ctx;
        this.cache = cache;
        wrapperFiles = new HashMap<>();
    }

    /**
     * Process a source type object
     * @param sourceType The source type to process
     * @throws WrapProcessException Invalid methods
     * @throws IOException Thrown by file writers
     */
    public void process(SourceTypeInfo sourceType) throws WrapProcessException, IOException {
        AccessFileGenerator accessGen = null;
        MutateFileGenerator mutateGen = null;

        if (sourceType.hasProperty(WrapType.ACCESSOR))
            accessGen = new AccessFileGenerator(ctx, sourceType, sourceType.getProperty(WrapType.ACCESSOR), cache);

        if (sourceType.hasProperty(WrapType.MUTATOR))
            mutateGen = new MutateFileGenerator(ctx, sourceType, sourceType.getProperty(WrapType.MUTATOR), cache);

        Iterable<MethodInfo> methods = sourceType.getMethods()::iterator;
        for (MethodInfo method : methods) {
            if (accessGen != null)
                accessGen.addMethod(method);
            if (mutateGen != null)
                mutateGen.addMethod(method);
        }

        // Write access file
        if (accessGen != null) {
            try (BufferedWriter writer = getWriter(accessGen.getOutputType(), sourceType)) {
                accessGen.writeFile(writer);
            }
        }

        // Write mutate file
        if (mutateGen != null) {
            try (BufferedWriter writer = getWriter(mutateGen.getOutputType(), sourceType)) {
                mutateGen.writeFile(writer);
            }
        }

        // Add helper to wrapper
        if (sourceType.hasProperty(WrapType.WRAPPER)) {
            OutputType wrapper = sourceType.getProperty(WrapType.WRAPPER);

            if (!wrapperFiles.containsKey(wrapper))
                wrapperFiles.put(wrapper, new WrapperFileGenerator(wrapper, cache));

            wrapperFiles.get(wrapper).addHelper(sourceType);
        }
    }

    /**
     * Indicate that no more source files need to be processed.
     */
    public void finish() throws IOException, WrapProcessException {
        // write wrapper files
        for (WrapperFileGenerator generator : wrapperFiles.values()) {
            try (BufferedWriter writer = getWriter(generator.getOutputType())) {
                generator.writeFile(writer);
            }
        }
    }

    private BufferedWriter getWriter(OutputType out, SourceTypeInfo source) throws IOException {
        JavaFileObject sourceFile = ctx.filer().createSourceFile(out.getQualifiedName(), source.asElement());
        return new BufferedWriter(new OutputStreamWriter(sourceFile.openOutputStream()));
    }

    private BufferedWriter getWriter(OutputType out) throws IOException {
        JavaFileObject sourceFile = ctx.filer().createSourceFile(out.getQualifiedName());
        return new BufferedWriter(new OutputStreamWriter(sourceFile.openOutputStream()));
    }
    
}
