package be.kabuno.wrapper;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;
import java.lang.annotation.Annotation;
import java.util.stream.Stream;

/**
 * A class that has all information about a certain method.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class MethodInfo {

    private final ProcessContext ctx;
    private final ExecutableElement method;

    /**
     * Create a new method.
     *
     * @param ctx The processing context.
     * @param method The method element.
     */
    public MethodInfo(ProcessContext ctx, ExecutableElement method) {
        this.ctx = ctx;
        this.method = method;
    }

    /**
     * Check if this method is public.
     * @return True if this method is public, false otherwise.
     */
    public boolean isPublic() {
        return method.getModifiers().contains(Modifier.PUBLIC);
    }

    /**
     * Check if this method is annotated with the given annotation type.
     * The given annotation type must already be accessible, i.e. it must
     * already have been compiled.
     *
     * @return True if this method has the given annotation, false
     *          otherwise.
     */
    public boolean hasAnnotation(Class<? extends Annotation> annotation) {
        return method.getAnnotation(annotation) != null;
    }

    /**
     * Get the name of the method.
     * @return The name of the method.
     */
    public String getName() {
        return method.getSimpleName().toString();
    }

    /**
     * Get the type info of the return type.
     * @return A type info instance.
     */
    public TypeInfo getReturnType() {
        return TypeInfo.of(ctx, method.getReturnType());
    }

    /**
     * Get the type infos of all arguments.
     * @return An array of type info instances.
     */
    public Stream<Parameter> getParams() {
        return method.getParameters().stream()
                .map(Parameter::new);
    }

    /**
     * Get all throw declarations.
     * @return An array of type info instances.
     */
    public Stream<TypeInfo> getThrows() {
        return method.getThrownTypes().stream()
                .map(t -> TypeInfo.of(ctx, t));
    }

    /**
     * Get the type parameters of this method.
     * @return An array of type info instances.
     */
    public Stream<TypeInfo> getTypeParams() {
        return method.getTypeParameters().stream()
                .map(p -> TypeInfo.of(ctx, p.asType()));
    }

    @Override
    public int hashCode() {
        return method.hashCode();
    }

    /**
     * Get this method's documentation.
     * @return A documentation string, null if there wasn't any documentation.
     */
    public String getDocs() {
        return ctx.elemUtils().getDocComment(method);
    }

    /**
     * Get the executable element associated with this method info.
     * @return An executable element instance.
     */
    public ExecutableElement asElement() {
        return method;
    }

    /**
     * A method parameter has a type and a name.
     */
    public class Parameter {
        public final TypeInfo type;
        public final String name;

        private Parameter(VariableElement e) {
            this.type = TypeInfo.of(ctx, e.asType());
            this.name = e.getSimpleName().toString();
        }
    }
}
