package be.kabuno.wrapper;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import java.nio.charset.Charset;

/**
 * A processing context with delegate methods and utility methods. This class contains
 * references to the objects offered by {@link ProcessingEnvironment}.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class ProcessContext {

    private final ProcessingEnvironment env;

    /** Create a new processing environment. */
    public ProcessContext(ProcessingEnvironment env) {
        this.env = env;
    }

    /**
     * Get the charset used to encode source files.
     *
     * TODO make this an option
     *
     * @return The charset used to encode source files.
     */
    public Charset getCharset() {
        return Charset.defaultCharset();
    }

    /**
     * Get the processing environment's filer.
     * @return A filer
     */
    public Filer filer() {
        return env.getFiler();
    }

    /**
     * Get the processing environment's element utilities object.
     * @return An elements instance
     */
    public Elements elemUtils() {
        return env.getElementUtils();
    }

    /**
     * Get the processing environment's type utilities object.
     * @return A types instance
     */
    public Types typeUtils() {
        return env.getTypeUtils();
    }

    /**
     * Get the processing environment's message.
     * @return A message instance
     */
    public Messager msg() {
        return env.getMessager();
    }

    /**
     * Print a note message.
     * @param message The message to print.
     */
    public void note(String message) {
        env.getMessager().printMessage(Diagnostic.Kind.NOTE, message);
    }

    /**
     * Print a note message.
     * @param message The message to print.
     * @param e The element associated with this message.
     */
    public void note(String message, Element e) {
        env.getMessager().printMessage(Diagnostic.Kind.NOTE, message, e);
    }

    /**
     * Print a warning message.
     * @param message The message to print.
     */
    public void warn(String message) {
        env.getMessager().printMessage(Diagnostic.Kind.WARNING, message);
    }

    /**
     * Print an error message.
     * @param message The message to print.
     * @param e The element associated with this message.
     */
    public void warn(String message, Element e) {
        env.getMessager().printMessage(Diagnostic.Kind.WARNING, message, e);
    }

    /**
     * Print an error message.
     * @param message The message to print.
     */
    public void error(String message) {
        env.getMessager().printMessage(Diagnostic.Kind.ERROR, message);
    }

    /**
     * Print an error message.
     * @param message The message to print.
     * @param e The element associated with this message.
     */
    public void error(String message, Element e) {
        env.getMessager().printMessage(Diagnostic.Kind.ERROR, message, e);
    }




}
