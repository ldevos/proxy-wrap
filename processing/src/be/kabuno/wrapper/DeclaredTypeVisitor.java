package be.kabuno.wrapper;

import javax.lang.model.type.DeclaredType;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import java.util.Optional;

import static be.kabuno.wrapper.TypeInfo.Declared;

/**
 * A visitor that further refines {@link be.kabuno.wrapper.TypeInfo.Visitor#visitDeclared(Declared)}.
 *
 * @param <R> The return type of the visitor
 * @author Laurens Devos
 * @since 1.0
 */
public abstract class DeclaredTypeVisitor<R> {

    /** The processing context. */
    protected final ProcessContext ctx;
    protected final SourceTypeCache cache;

    private final DeclaredType listType;
    private final DeclaredType streamType;

    /**
     * Create a new declared type visitor.
     *
     * @param context The processing context.
     */
    protected DeclaredTypeVisitor(ProcessContext context, SourceTypeCache cache) {
        this.ctx = context;
        this.cache = cache;

        Elements els = ctx.elemUtils();

        // TODO cache these somehow
        listType     = (DeclaredType) els.getTypeElement(java.util.List.class.getCanonicalName()).asType();
        streamType   = (DeclaredType) els.getTypeElement(java.util.stream.Stream.class.getCanonicalName()).asType();
    }

    /**
     * Method that delegates to the specific visit handlers.
     *
     * @param declared The type to visit.
     *
     * @return Something returned by the specific visitor.
     * @throws WrapProcessException incorrect types
     */
    public final R delegateDeclared(Declared declared) throws WrapProcessException {
        Types types = ctx.typeUtils();

        if (declared.isWrapType())
            return visitWrapType(cache.get(declared.asElement()));

        if (types.isAssignable(ctx.typeUtils().erasure(declared.getTypeMirror()), listType)) {
            Optional<TypeInfo.Generic> param = declared.getTypeParams()
                    .filter(g -> g.getBound().isWrapType()).findFirst();

            if (param.isPresent())
                return visitWrapTypeList(declared, cache.get(param.get().getBound().asElement()));
        }

        if (types.isAssignable(ctx.typeUtils().erasure(declared.getTypeMirror()), streamType)) {
            Optional<TypeInfo.Generic> param = declared.getTypeParams()
                    .filter(g -> g.getBound().isWrapType()).findFirst();

            if (param.isPresent())
                return visitWrapTypeStream(declared, cache.get(param.get().getBound().asElement()));
        }

        return visitGeneral(declared);
    }

    /**
     * Method for types annotated with the {@link be.kabuno.wrapper.WrapType WrapType}
     * annotation.
     * <p> Default implementation delegates to {@link #visitGeneral(Declared)}</p>
     *
     * @param wrapType The wrap type.
     * @return Some return value
     */
    public R visitWrapType(SourceTypeInfo wrapType) {
        return visitGeneral(wrapType);
    }

    /**
     * Method for {@link java.util.List} declared types that have wrap-type elements.
     * <p> It is assured that</p>
     * <ul>
     *     <li> <tt>list</tt> represents a java {@link java.util.List List}</li>
     *     <li> <tt>list.{@link Declared#getTypeParams() getTypeParams()}</tt> returns a single value</li>
     * </ul>
     * <p> Default implementation delegates to {@link #visitGeneral(Declared)}</p>
     *
     * @param list The list
     * @param elemType The type of the elements in the list
     * @return A return value
     */
    public R visitWrapTypeList(Declared list, SourceTypeInfo elemType) {
        return visitGeneral(list);
    }

    /**
     * Method for {@link java.util.stream.Stream} declared types.
     * <p> It is assured that</p>
     * <ul>
     *     <li> <tt>stream</tt> represents a java {@link java.util.stream.Stream Stream}</li>
     *     <li> <tt>stream.{@link Declared#getTypeParams() getTypeParams()}</tt> returns a single value</li>
     * </ul>
     * <p> Default implementation delegates to {@link #visitGeneral(Declared)}</p>
     *
     * @param stream The stream
     * @param elemType The type of the elements in the stream.
     * @return A return value
     */
    public R visitWrapTypeStream(Declared stream, SourceTypeInfo elemType) {
        return visitGeneral(stream);
    }

    /**
     * Method for {@link java.util.Map} declared types.
     * <p> It is assured that</p>
     * <ul>
     *     <li> <tt>map</tt> represents a java {@link java.util.Map Stream}</li>
     *     <li> <tt>map.{@link Declared#getTypeParams() getTypeParams()}</tt> returns two values</li>
     * </ul>
     * <p> Default implementation delegates to {@link #visitGeneral(Declared)}</p>
     *
     * @param map The stream
     * @return A return value
     */
    // public R visitMap(Declared map) {
    //     return visitGeneral(map);
    // }

    /**
     * Method for {@link Iterable} declared types.
     * <p> It is assured that</p>
     * <ul>
     *     <li> <tt>iterable</tt> represents a java {@link Iterable}</li>
     *     <li> <tt>iterable.{@link Declared#getTypeParams() getTypeParams()}</tt> returns a single value</li>
     * </ul>
     * <p> Default implementation delegates to {@link #visitGeneral(Declared)}</p>
     *
     * @param iterable The stream
     * @return A return value
     */
    // public R visitIterable(Declared iterable) {
    //     return visitGeneral(iterable);
    // }

    /**
     * Visit a general declared type that did not match with any of the specific
     * visit cases or it was not of interest (the specific method was not overloaded
     * and delegated to this).
     *
     * @param declared The declared type.
     * @return Some return value
     */
    public abstract R visitGeneral(Declared declared);

}
