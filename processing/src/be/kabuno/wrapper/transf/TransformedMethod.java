package be.kabuno.wrapper.transf;

import be.kabuno.wrapper.MethodInfo;
import be.kabuno.wrapper.WrapProcessException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * A method transformation is a
 * <ul>
 *     <li>return type transformation</li>
 *     <li>parameter type transformations</li>
 * </ul>
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class TransformedMethod {

    private final MethodInfo method;
    private final TypeTransformer returnTypeTransformer;
    private final TypeTransformer paramTypeTransformer;

    /**
     * Create a method transformation object.
     * @param method The subject method
     */
    public TransformedMethod(MethodInfo method, TypeTransformer returnTypeTransformer,
                             TypeTransformer paramTypeTransformer) {
        this.method = method;
        this.returnTypeTransformer = returnTypeTransformer;
        this.paramTypeTransformer = paramTypeTransformer;
    }

    /**
     * Get the original method.
     * @return The original method
     */
    public MethodInfo getMethod() {
        return method;
    }

    /**
     * Get the parameters of this method.
     * @return A stream of parameters.
     */
    public List<Parameter> getTransformedParams() throws WrapProcessException {
        ArrayList<Parameter> params = new ArrayList<>();
        Iterable<MethodInfo.Parameter> it = method.getParams()::iterator;
        for (MethodInfo.Parameter p : it) {
            params.add(new Parameter(p.name, paramTypeTransformer.transform(p.type, method.asElement())));
        }
        return params;
    }

    /**
     * Get the transformed return type.
     * @return The transformed return type.
     */
    public TransformedType getTransformedReturnType() throws WrapProcessException {
        return returnTypeTransformer.transform(method.getReturnType(), method.asElement());
    }

    /**
     * Get a stream of pieces of a call of the method.
     * @return A stream of pieces.
     */
    public Stream<String> getMethodCall() throws WrapProcessException {
        ArrayList<String> pieces = new ArrayList<>();
        pieces.add("original." + method.getName() + '(');

        boolean first = true;
        for (Parameter p : getTransformedParams()) {
            if (!first) pieces.add(", ");
            first = false;
            p.type.convert(p.name).forEach(pieces::add);
        }

        pieces.add(")");
        return pieces.stream();
    }

    /**
     * A class that represents a transformed parameter.
     */
    public static final class Parameter {
        public final String name;
        public final TransformedType type;

        private Parameter(String name, TransformedType type) {
            this.name = name;
            this.type = type;
        }
    }
}
