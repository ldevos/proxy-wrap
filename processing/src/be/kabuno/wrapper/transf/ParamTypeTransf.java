package be.kabuno.wrapper.transf;

import be.kabuno.wrapper.*;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

import static be.kabuno.codegen.SourceWriter.REPLACE;
import static be.kabuno.codegen.SourceWriter.TYPE;

/**
 * A type transformation for method parameters.
 * <ul>
 *     <li>WrapTypes are unwrapped</li>
 * </ul>
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class ParamTypeTransf implements TypeTransformer {

    private final ProcessContext ctx;
    private final SourceTypeCache cache;

    /**
     * Create a new parameter type transformer.
     * @param ctx The processing context.
     * @param cache The source type cache.
     */
    public ParamTypeTransf(ProcessContext ctx, SourceTypeCache cache) {
        this.ctx = ctx;
        this.cache = cache;
    }

    @Override
    public TypeInfo.Visitor<TransformedType> getVisitor(Element subject) {
        return new Visitor(subject);
    }

    private class Visitor
            extends DeclaredTypeVisitor<TransformedType>
            implements TypeInfo.Visitor<TransformedType> {

        private final TransformedType tt = new TransformedType();
        private final Element subject;

        public Visitor(Element subject) {
            super(ParamTypeTransf.this.ctx, ParamTypeTransf.this.cache);
            this.subject = subject;
        }

        // -- DECLARED TYPE VISITOR --

        @Override
        public TransformedType visitWrapType(SourceTypeInfo wrapType) {
            if (!wrapType.hasProperty(WrapType.ACCESSOR))
                return visitGeneral(wrapType);

            OutputType out = wrapType.getProperty(WrapType.ACCESSOR);
            tt.setTypes(wrapType.getCodeName(), out.getCodeName());
            tt.setConversion(TYPE, out.getQualifiedName(), ".unwrap(", REPLACE, ", lock)");
            return tt;
        }

        @Override
        public TransformedType visitWrapTypeList(TypeInfo.Declared list, SourceTypeInfo elemType) {
            tt.setException(subject, "List of wrap-types as argument not allowed.");
            return tt;
        }

        @Override
        public TransformedType visitWrapTypeStream(TypeInfo.Declared stream, SourceTypeInfo elemType) {
            tt.setException(subject, "Stream of wrap-types as argument not allowed.");
            return tt;
        }

        @Override
        public TransformedType visitGeneral(TypeInfo.Declared declared) {
            tt.setTypes(declared.getCodeName());
            return tt;
        }

        // -- TYPE INFO TYPE VISITOR --


        @Override
        public TransformedType visitDeclared(TypeInfo.Declared declared) {
            try {
                return delegateDeclared(declared); // delegate to declared type visitor
            } catch (WrapProcessException e) {
                tt.setException(e);
                return tt;
            }
        }

        @Override
        public TransformedType visitPrimitive(TypeInfo.Primitive primitive) {
            tt.setTypes(primitive.getCodeName());
            return tt;
        }

        @Override
        public TransformedType visitDeclaredArray(TypeInfo.DeclaredArray array) {
            tt.setException(subject, "object arrays not allowed");
            return tt;
        }

        @Override
        public TransformedType visitPrimitiveArray(TypeInfo.PrimitiveArray array) {
            tt.setTypes(array.getCodeName());
            return tt;
        }

        @Override
        public TransformedType visitUnknown(TypeMirror type) {
            tt.setException(subject, "cannot deal with parameter type '" + type.toString() + "'");
            return tt;
        }
    }
}
