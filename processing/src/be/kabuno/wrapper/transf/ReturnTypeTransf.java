package be.kabuno.wrapper.transf;

import be.kabuno.wrapper.*;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

import static be.kabuno.codegen.SourceWriter.*;

/**
 * A return type transformation.
 * <ul>
 *     <li>WrapTypes are wrapped</li>
 *     <li>void return types are not allowed</li>
 *     <li>Lists are wrapped as WrapLists</li>
 *     <li>Arrays as return type are not allowed</li>
 * </ul>
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class ReturnTypeTransf implements TypeTransformer {


    private final ProcessContext ctx;
    private final SourceTypeCache cache;

    public ReturnTypeTransf(ProcessContext ctx, SourceTypeCache cache) {
        this.ctx = ctx;
        this.cache = cache;
    }

    @Override
    public TypeInfo.Visitor<TransformedType> getVisitor(Element subject) {
        return new Visitor(subject);
    }

    private class Visitor
            extends DeclaredTypeVisitor<TransformedType>
            implements TypeInfo.Visitor<TransformedType> {

        private final Element subject;
        private final TransformedType tt = new TransformedType();

        public Visitor(Element subject) {
            super(ReturnTypeTransf.this.ctx, ReturnTypeTransf.this.cache);
            this.subject = subject;
        }

        // -- DECLARED TYPE VISITOR --

        @Override
        public TransformedType visitGeneral(TypeInfo.Declared declared) {
            tt.setTypes(declared.getCodeName());
            return tt;
        }

        @Override
        public TransformedType visitWrapType(SourceTypeInfo wrapType) {
            if (!wrapType.hasProperty(WrapType.ACCESSOR)) {
                tt.setTypes(wrapType.getCodeName());
                return tt;
            }

            // The type in sourceType is an access-wrapped type
            OutputType output = wrapType.getProperty(WrapType.ACCESSOR);
            tt.setTypes(wrapType.getCodeName(), output.getCodeName());
            tt.setConversion(TYPE, output.getQualifiedName(), ".wrap(", REPLACE, ", lock)");
            return tt;
        }

        @Override
        public TransformedType visitWrapTypeList(TypeInfo.Declared list, SourceTypeInfo elemType) {
            if (!elemType.hasProperty(WrapType.ACCESSOR)) {
                tt.setConversion(list.getCodeName());
                return tt;
            }

            OutputType out = elemType.getProperty(WrapType.ACCESSOR);

            tt.setTypes(list.getCodeName(), WrapList.class.getCanonicalName() + '<' + elemType.getCodeName() +
                    ", " + out.getCodeName() + '>');
            tt.setConversion("new ", TYPE, WrapList.class.getCanonicalName(),
                    "<", TYPE, elemType.getCodeName(), ", ", TYPE, out.getCodeName(), ">(",
                    INDENT, INDENT, NEWLINE,
                    REPLACE, ", ", NEWLINE,
                    TYPE, WrapperBase.class.getCanonicalName(), ".curryWrapFn(",
                    TYPE, out.getQualifiedName(), "::wrap, lock))",
                    UN_INDENT, UN_INDENT);
            return tt;
        }
        // -- TYPE INFO VISITOR --

        @Override
        public TransformedType visitDeclared(TypeInfo.Declared declared) {
            try {
                return delegateDeclared(declared); // Delegate to declared type visitor
            } catch (WrapProcessException e) {
                tt.setException(e);
                return tt;
            }
        }

        @Override
        public TransformedType visitDeclaredArray(TypeInfo.DeclaredArray array) {
            tt.setException(subject, "object arrays not allowed as return type");
            return tt;
        }

        @Override
        public TransformedType visitPrimitiveArray(TypeInfo.PrimitiveArray array) {
            tt.setException(subject, "primitive arrays not allowed");
            return tt;
        }

        @Override
        public TransformedType visitVoid(TypeMirror type) {
            tt.setException(subject, "an accessor should return a value");
            return tt;
        }

        @Override
        public TransformedType visitPrimitive(TypeInfo.Primitive primitive) {
            tt.setTypes(primitive.getCodeName());
            return tt;
        }

        @Override
        public TransformedType visitUnknown(TypeMirror type) {
            tt.setException(subject, "cannot deal with return type '" + type.toString() + "'");
            return tt;
        }
    }
}
