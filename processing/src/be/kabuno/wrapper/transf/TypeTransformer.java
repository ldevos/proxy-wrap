package be.kabuno.wrapper.transf;

import be.kabuno.wrapper.TypeInfo;
import be.kabuno.wrapper.WrapProcessException;

import javax.lang.model.element.Element;

/**
 * A type transformer transforms a type into another type.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public interface TypeTransformer {

    /**
     * Transform the given type into a transformed type.
     *
     * @param type the type to transform
     * @param subject The subject of this visit: used for error reporting, use an element
     *                that would point the user to a sensible area in the code if something
     *                goes wrong.
     * @return A transformed type instance.
     * @throws WrapProcessException The transformation was not successful.
     */
    default TransformedType transform(TypeInfo type, Element subject) throws WrapProcessException {
        TransformedType res = type.accept(getVisitor(subject));

        if (res.hasException())
            throw res.getException();

        return res;
    }


    /**
     * Get the visitor used by this type transformer.
     *
     * @param subject The subject of this visit: used for error reporting, use an element
     *                that would point the user to a sensible area in the code if something
     *                goes wrong.
     * @return A visitor implementation
     */
    TypeInfo.Visitor<TransformedType> getVisitor(Element subject);
}
