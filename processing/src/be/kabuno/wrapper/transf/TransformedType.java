package be.kabuno.wrapper.transf;

import be.kabuno.wrapper.WrapProcessException;

import javax.lang.model.element.Element;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;

import static be.kabuno.codegen.SourceWriter.REPLACE;

/**
 * A class that represents a transformed type. A transformed type knows
 * <ul>
 *     <li>its original type</li>
 *     <li>its new transformed type</li>
 *     <li>a function that can transform expressions of the first type to the second</li>
 * </ul>
 * A transformed type can be in an error state. If it is, all its accessor method
 * will thrown an exception.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class TransformedType {

    private WrapProcessException exception = null;
    private String orig;
    private String transf;
    private Function<Stream<String>, Stream<String>> conv = Function.identity();

    public TransformedType() {}

    /**
     * Get an identity transformation.
     * @param type The type for both the original and the transformed
     */
    public TransformedType(String type) {
        this.orig = type;
        this.transf = type;
    }

    /**
     * Set the type transformation exception. This exception is thrown on
     * the getter methods.
     *
     * @param ex The exception
     */
    void setException(WrapProcessException ex) {
        this.exception = ex;
    }

    /**
     * Set the type transformation exception.
     *
     * @param el The element that caused the trouble.
     * @param msg The error message.
     * @see #setException(WrapProcessException)
     */
    void setException(Element el, String msg) {
        setException(new WrapProcessException(el, msg));
    }

    /**
     * Set the types of this type transformation.
     * @param orig The original type name, fully qualified
     * @param transf The transformed type name, fully qualified
     */
    void setTypes(String orig, String transf) {
        this.orig = orig;
        this.transf = transf;
    }

    /**
     * Set the types of this type transformation.
     * @param type The type, which is used both as the original type and the transformed type (no transformation)
     */
    void setTypes(String type) {
        this.orig = type;
        this.transf = type;
    }

    /**
     * Set the type transformation.
     * @param pieces A number of pieces, use the {@link be.kabuno.codegen.SourceWriter#REPLACE} indicator to indicate
     *               where the expression that evaluates to the original type should go.
     */
    void setConversion(String... pieces) {
        conv = str -> Arrays.stream(pieces).flatMap(p -> (p == REPLACE) ? str : Stream.of(p));
    }

    /**
     * Get the exception on this transformed type.
     * @return An exception or null.
     */
    public WrapProcessException getException() {
        return exception;
    }

    /**
     * Return whether or not this exception is in an error state.
     * @return True if in error state, false otherwise.
     */
    public boolean hasException() {
        return exception != null;
    }

    /**
     * Get the original type.
     * @return The fully qualified original type name.
     */
    public String getOrig() {
        return orig;
    }

    /**
     * Get the transformed type.
     * @return The fully qualified transformed type name.
     */
    public String getTransf() {
        return transf;
    }

    /**
     * @see #convert(Stream)
     */
    public Stream<String> convert(String expr) {
        return convert(Stream.of(expr));
    }

    /**
     * Convert the given expression that evaluates to the original type to
     * an expression that evaluates to the transformed type.
     * @param expr The expression to convert as a stream of pieces
     * @return A stream of pieces that represents an expression that evaluates
     *          to the transformed type.
     */
    public Stream<String> convert(Stream<String> expr) {
        return conv.apply(expr);
    }
}
