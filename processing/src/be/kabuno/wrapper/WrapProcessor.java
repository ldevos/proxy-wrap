package be.kabuno.wrapper;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Annotation processor for
 * <ul>
 *     <li>{@link WrapType}</li>
 *     <li>{@link Access}</li>
 *     <li>{@link Mutate}</li>
 * </ul>
 * <p> Thank you, Hannes Dorfmann, for
 * <a href="http://hannesdorfmann.com/annotation-processing/annotationprocessing101">your informative blog</a>.</p>
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class WrapProcessor extends AbstractProcessor {

    private ProcessContext ctx;
    private SourceTypeCache cache;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        ctx = new ProcessContext(processingEnv);
        cache = new SourceTypeCache(ctx);
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> wrapElements = roundEnv.getElementsAnnotatedWith(WrapType.class);

        SourceTypeProcessor processor = new SourceTypeProcessor(ctx, cache);

        for (Element wrapElement : wrapElements) {
            try {
                SourceTypeInfo sourceType = cache.get(wrapElement);
                processor.process(sourceType);
            } catch (Throwable e) {
                dealWithError(e, wrapElement);
            }
        }

        try {
            processor.finish();
        } catch (Exception e) {
            dealWithError(e, null);
        }

        return false;
    }

    private void dealWithError(Throwable ex, Element problem) {
        StringWriter strwr = new StringWriter();
        PrintWriter writer = new PrintWriter(strwr);
        writer.flush();
        ex.printStackTrace(writer);
        if (ex instanceof WrapProcessException)
            ctx.error("WrapType annotation processing error:\n" + strwr.toString(),
                    ((WrapProcessException) ex).getElement());
        else if (problem != null)
            ctx.error("UNEXPECTED ERROR during annotation processing:\n" + strwr.toString(), problem);
        else
            ctx.error("UNEXPECTED ERROR during annotation processing:\n" + strwr.toString());

        throw new RuntimeException("preprocessor error: " + strwr.toString(), ex);
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_8;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return new LinkedHashSet<>(Arrays.asList(
                WrapType.class.getCanonicalName(),
                Access.class.getCanonicalName(),
                Mutate.class.getCanonicalName()));
    }
}
