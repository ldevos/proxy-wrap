package be.kabuno.wrapper;

import javax.lang.model.element.*;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import java.util.*;
import java.util.stream.Stream;

/**
 * A source type is a special kind of {@link Declared} file type.
 * If you have an instance of this class, you are certain that the type
 * associated with this class is a {@link #isValidWrapType(Element) valid}
 * {@link WrapType}-annotated type.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public class SourceTypeInfo extends TypeInfo.Declared {

    public enum Kind {
        /** A concrete class. */
        CONCRETE,

        /** An abstract class. */
        ABSTRACT,

        /** An interface. */
        INTERFACE
    }

    private final ProcessContext ctx;
    private final SourceTypeCache cache;
    private final TypeElement typeElement;
    private final Map<String, OutputType> wrapTypeProps;
    private Set<MethodInfo> methodSet = null;

    /**
     * Create a new typeElement type.
     * <p> Use {@link SourceTypeCache#get(Element)} to create instances.</p>
     *
     * @param ctx The processing context.
     * @param cache The source type cache
     * @param typeElement The type element this source type is based on.
     */
    SourceTypeInfo(ProcessContext ctx, SourceTypeCache cache, TypeElement typeElement)
            throws WrapProcessException {
        super(ctx, (DeclaredType) typeElement.asType());
        this.cache = cache;

        if (!isValidWrapType(typeElement))
            throw new WrapProcessException(typeElement, "Invalid use of " + WrapType.class.getSimpleName() +
                    " annotation. Type must be a top-level, public (abstract) class or interface.");

        this.ctx = ctx;
        this.typeElement = Objects.requireNonNull(typeElement);

        this.wrapTypeProps = getWrapTypeAnnotationProperties();
    }

    /**
     * Return whether the given element has the type of a valid wrap-type.
     * A wrap-type is valid when:
     * <ul>
     *     <li> it has the {@link WrapType} annotation</li>
     *     <li> it is a class or interface</li>
     *     <li> it is public</li>
     *     <li><s> it is top-level</s></li>
     *     <li> the properties of the {@link WrapType} are either not present,
     *     or valid</li>
     * </ul>
     * @param element The element to test
     * @return True if the given type info represents a valid wrap-type, false otherwise.
     */
    public static boolean isValidWrapType(Element element) {
        // Must be class or interface
        if (!element.getKind().equals(ElementKind.CLASS) && !element.getKind().equals(ElementKind.INTERFACE))
            return false;

        TypeElement typeElement = (TypeElement) element;

        // Must be public
        if (!typeElement.getModifiers().contains(Modifier.PUBLIC))
            return false;

        // Must be top-level
        // if (!NestingKind.TOP_LEVEL.equals(typeElement.getNestingKind()))
        //     return false;

        // Must have WrapType annotation
        if (typeElement.getAnnotation(WrapType.class) == null)
            return false;

        // All conditions are met
        return true;
    }

    private Map<String, OutputType> getWrapTypeAnnotationProperties()
            throws WrapProcessException {
        HashMap<String, OutputType> map = new HashMap<>();
        TypeMirror mirror = ctx.elemUtils().getTypeElement(WrapType.class.getCanonicalName()).asType();

        AnnotationMirror annotation = typeElement.getAnnotationMirrors().stream()
                .filter(a -> ctx.typeUtils().isSameType(mirror, a.getAnnotationType()))
                .findAny().orElseThrow(() -> new WrapProcessException(typeElement, "no wraptype annotation"));

        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotation.getElementValues().entrySet()) {
            map.put(entry.getKey().getSimpleName().toString(),
                    new OutputType(ctx, typeElement, entry.getValue().getValue().toString()));
        }

        return map;
    }

    /**
     * Check if the {@link WrapType} annotation of this class has a certain property.
     * @param property A property name.
     * @return True if it has the given property, false otherwise.
     */
    public boolean hasProperty(String property) {
        return wrapTypeProps.containsKey(property);
    }

    /**
     * Get a property.
     * @param property A property name.
     * @return The property value if it exists, or null otherwise.
     */
    public OutputType getProperty(String property) {
        return wrapTypeProps.get(property);
    }

    /**
     * Get the wrap-type super type of this type, if it has one. The super type mustn't
     * be direct.
     * @return An optional super wrap-type.
     */
    public Optional<SourceTypeInfo> getWrapTypeSuperClass() throws WrapProcessException {
        TypeElement superElem = typeElement;
        while (superElem.getSuperclass().getKind().equals(TypeKind.DECLARED)) {
            superElem = (TypeElement) ((DeclaredType) superElem.getSuperclass()).asElement();
            if (isValidWrapType(superElem)) {
                return Optional.of(cache.get(superElem));
            }
        }
        return Optional.empty();
    }

    /**
     * Get all implemented wrap-type interfaces.
     * @return A stream of wrap type info instances.
     */
    public Stream<SourceTypeInfo> getWrapTypeInterfaces() throws WrapProcessException {
        List<? extends TypeMirror> interfaces = typeElement.getInterfaces();
        Iterator<Element> it = interfaces.stream()
                .<Element>map(ctx.typeUtils()::asElement)
                .filter(SourceTypeInfo::isValidWrapType)
                .iterator();

        ArrayList<SourceTypeInfo> list = new ArrayList<>(interfaces.size());

        while (it.hasNext())
            list.add(cache.get(it.next()));
        return list.stream();
    }

    /**
     * Return a stream of methods defines on this source type.
     * If this type is abstract, then all methods of super interfaces are also returned,
     * else only the methods this type defines are returned.
     * @return A stream of methods
     */
    public Stream<MethodInfo> getMethods() {
        if (methodSet == null)
            initMethodSet();

        return methodSet.stream();
    }

    private void initMethodSet() {
        methodSet = new LinkedHashSet<>();

        // If this is not a concrete class, it may have
        // implicit methods. We want them all
        if (!getKind().equals(Kind.CONCRETE)) {
            ctx.elemUtils().getAllMembers(typeElement).stream()
                    .filter(m -> ElementKind.METHOD.equals(m.getKind()))
                    .map(ExecutableElement.class::cast)
                    .map(e -> new MethodInfo(ctx, e))
                    .forEach(m -> methodSet.add(m));
        } else {
            typeElement.getEnclosedElements().stream()
                    .filter(m -> ElementKind.METHOD.equals(m.getKind()))
                    .map(ExecutableElement.class::cast)
                    .map(e -> new MethodInfo(ctx, e))
                    .forEach(m -> methodSet.add(m));
        }

        methodSet = Collections.unmodifiableSet(methodSet);
    }

    public Set<MethodInfo> getMethodSet() {
        if (methodSet == null)
            initMethodSet();
        return methodSet;
    }

    /**
     * Get the inner wrap classes of this type.
     * An inner wrap class is a concrete class that extends/implements the main type
     * and that at least has one method annotated with {@link Access}.
     *
     * @return A stream of inner classes.
     */
    public Stream<SourceInnerTypeInfo> getInnerClasses() {
        return typeElement.getEnclosedElements().stream()
                .filter(e -> e.getKind().equals(ElementKind.CLASS))
                .map(TypeElement.class::cast)
                .filter(e -> ctx.typeUtils().isAssignable(e.asType(), typeElement.asType()))
                .filter(e -> !e.getModifiers().contains(Modifier.ABSTRACT))
                .map(e -> new SourceInnerTypeInfo(ctx, this, e))
                .filter(i -> i.getMethods().count() > 0);
    }

    /**
     * Get the kind of typeElement file.
     * @return The kind of typeElement file.
     */
    public Kind getKind() {
        if (typeElement.getKind().equals(ElementKind.CLASS)) {
            if (typeElement.getModifiers().contains(Modifier.ABSTRACT)) {
                return Kind.ABSTRACT;
            } else {
                return Kind.CONCRETE;
            }
        }
        else if (typeElement.getKind().equals(ElementKind.INTERFACE)) {
            return Kind.INTERFACE;
        }

        throw new AssertionError("illegal type element kind");
    }
}
