package be.kabuno.wrapper;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.*;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class that contains info about and utility methods for a type mirror.
 *
 * @author Laurens Devos
 * @since 1.0
 */
public abstract class TypeInfo {

    /** Create a type info instance. */
    public static TypeInfo of(ProcessContext ctx, TypeMirror type) {
        switch (type.getKind()) {
        case DECLARED:
            return new Declared(ctx, (DeclaredType) type);
        case WILDCARD:
            return new Generic(ctx, type);
        case BOOLEAN:
        case BYTE:
        case CHAR:
        case DOUBLE:
        case FLOAT:
        case INT:
        case LONG:
        case SHORT:
            return new Primitive(ctx, (PrimitiveType) type);
        case ARRAY:
            ArrayType arr = (ArrayType) type;
            if (arr.getComponentType().getKind().isPrimitive())
                return new PrimitiveArray(ctx, arr);
            else if (arr.getComponentType().getKind().equals(TypeKind.DECLARED))
                return new DeclaredArray(ctx, arr);
            throw new IllegalStateException("array with component of type kind " +
                    arr.getComponentType().getKind());
        default:
            return new Other(type);
        }
    }

    /**
     * The the underlying type mirror.
     * @return The underlying type mirror.
     */
    public abstract TypeMirror getTypeMirror();

    /**
     * Get the simple name of this type.
     * @return The simple.
     */
    public String getName() {
        return getTypeMirror().toString();
    }

    /**
     * Get the name of the type as it is used in source code. This name
     * includes generic type information.
     * @return The name of the type as it is used in source code.
     */
    public abstract String getCodeName();

    /**
     * If this is a declared type, then the returned optional will contain
     * this declared type.
     * @return Optional empty if this type is not declared, a declared version
     *                  of this type otherwise.
     */
    public Optional<Declared> asDeclared() {
        return Optional.empty();
    }

    /**
     * Visit this type with a {@link Visitor}.
     * @param visitor A visitor implementation.
     * @param <R> The return type of the visitor.
     * @return Whatever is returned by the visitor.
     */
    public abstract <R> R accept(Visitor<R> visitor);

    @Override
    public String toString() {
        return String.format("%s{%s}", getClass().getName(), getCodeName());
    }

    @Override
    public int hashCode() {
        int result = 97;
        result = 37 * result + getTypeMirror().hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof TypeInfo))
            return false;

        TypeInfo other = (TypeInfo) obj;
        return getTypeMirror().equals(other.getTypeMirror());
    }

















    /**
     * Type info for declared types.
     * @see javax.lang.model.type.TypeKind#DECLARED
     * @see DeclaredType
     */
    public static class Declared extends TypeInfo {

        private final ProcessContext ctx;
        private final DeclaredType type;

        public Declared(ProcessContext ctx, DeclaredType type) {
            this.ctx = ctx;
            this.type = type;
        }

        /**
         * Return whether this is a wrap type.
         *
         * @return True if this type is annotated with the {@link WrapType}
         * annotation, false otherwise.
         */
        public boolean isWrapType() {
            return asElement().getAnnotation(WrapType.class) != null;
        }

        /**
         * Get the element associated with this type.
         * @return A type element.
         */
        public TypeElement asElement() {
            return (TypeElement) type.asElement();
        }

        @Override
        public String getCodeName() {
            String params = getGenerics();
            return getQualifiedName() + (params.isEmpty() ? "" : '<' + params + '>');
        }

        /**
         * Get the generics string (w/o &lt; and &gt;).
         * @return A generics string
         */
        public String getGenerics() {
            return getTypeParams().map(Generic::getCodeName).collect(Collectors.joining(", "));
        }

        /**
         * Get the fully qualified name to this type (w/o generics).
         * @return The fully qualified name of this type.
         */
        public String getQualifiedName() {
            return asElement().getQualifiedName().toString();
        }

        /**
         * Get the package of this type.
         *
         * @return The package as a string.
         */
        public String getPackage() {
            return ctx.elemUtils().getPackageOf(type.asElement())
                    .getQualifiedName().toString();
        }

        @Override
        public DeclaredType getTypeMirror() {
            return type;
        }

        @Override
        public String getName() {
            return type.asElement().getSimpleName().toString();
        }

        /**
         * Get a stream of type parameters.
         * @return A stream of type parameters
         */
        public Stream<Generic> getTypeParams() {
            return type.getTypeArguments().stream().map(t -> new Generic(ctx, t));
        }

        @Override
        public Optional<Declared> asDeclared() {
            return Optional.of(this);
        }

        @Override
        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitDeclared(this);
        }
    }

    /**
     * Type info that represents a generic parameter. This can either be a wildcard type
     * or a declared type.
     *
     * Union types are currently not supported.
     *
     * @see TypeKind#DECLARED
     * @see TypeKind#WILDCARD
     */
    public static class Generic extends TypeInfo {

        private final ProcessContext ctx;
        private final TypeMirror typeMirror;

        private Generic(ProcessContext ctx, TypeMirror typeMirror) {
            this.ctx = ctx;
            this.typeMirror = typeMirror;
        }

        /**
         * Test whether this generic type parameter is a wildcard type.
         * @return True if this represents a wildcard type, false otherwise.
         */
        public boolean isWildcard() {
            return typeMirror.getKind().equals(TypeKind.WILDCARD);
        }

        /**
         * Get the declared parameter of this generic type. It is either the upper bound,
         * lower bound or just the type if not a wildcard type.
         * @return The bound.
         */
        public Declared getBound() {
            return TypeInfo.of(ctx, typeMirror).asDeclared()
                    .orElseThrow(() -> new RuntimeException("wildcard generic parameter type not supported"));
        }

        /**
         * Get the extends bound.
         * @return The extends bound.
         */
        public Optional<Declared> getUpperBound() {
            if (isWildcard()) {
                TypeMirror bound = ((WildcardType) typeMirror).getExtendsBound();
                if (bound == null)
                    return Optional.empty();
                return TypeInfo.of(ctx, bound).asDeclared();
            }
            return TypeInfo.of(ctx, typeMirror).asDeclared();
        }

        /**
         * Get the super bound.
         * @return The super bound.
         */
        public Optional<Declared> getLowerBound() {
            if (isWildcard()) {
                TypeMirror bound = ((WildcardType) typeMirror).getSuperBound();
                if (bound == null)
                    return Optional.empty();
                return TypeInfo.of(ctx, bound).asDeclared();
            }
            return TypeInfo.of(ctx, typeMirror).asDeclared();
        }

        @Override
        public TypeMirror getTypeMirror() {
            return typeMirror;
        }

        @Override
        public String getCodeName() {
            Optional<Declared> upperBound = getUpperBound();
            Optional<Declared> lowerBound = getLowerBound();

            if (isWildcard()) {
                if (upperBound.isPresent())
                    return "? extends " + upperBound.get().getCodeName();
                if (lowerBound.isPresent())
                    return "? super " + lowerBound.get().getCodeName();
            }

            return getUpperBound().map(Declared::getCodeName).orElse(typeMirror.toString());
        }

        @Override
        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitGeneric(this);
        }
    }

    /**
     * Type info for primitive types.
     * @see TypeKind#isPrimitive()
     * @see PrimitiveType
     */
    public static class Primitive extends TypeInfo {

        private final ProcessContext ctx;
        private final PrimitiveType type;

        /**
         * Create a new primitive type info object.
         * @param ctx The processing context.
         * @param type The type mirror.
         * @throws IllegalArgumentException Kind of type is not primitive
         */
        public Primitive(ProcessContext ctx, PrimitiveType type) {
            this.ctx = ctx;
            this.type = type;
        }

        public Declared getBoxed() {
            return new Declared(ctx, (DeclaredType) ctx.typeUtils().boxedClass(type).asType());
        }

        @Override
        public PrimitiveType getTypeMirror() {
            return type;
        }

        @Override
        public String getCodeName() {
            return type.toString();
        }

        @Override
        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitPrimitive(this);
        }
    }

    /**
     * An abstract array type.
     * @see TypeKind#ARRAY
     * @see ArrayType
     */
    public abstract static class Array extends TypeInfo {

        /**
         * Get the type info of the component type.
         * @return The type info of the component type.
         */
        public abstract TypeInfo getComponentType();

        @Override
        public abstract ArrayType getTypeMirror();

        @Override
        public String getCodeName() {
            return getComponentType().getCodeName() + "[]";
        }

        @Override
        public String getName() {
            return getComponentType().getName() + "[]";
        }
    }

    /** An array with primitive elements. */
    public static class PrimitiveArray extends Array {

        private final ProcessContext ctx;
        private final ArrayType type;

        PrimitiveArray(ProcessContext ctx, ArrayType type) {
            if (!type.getComponentType().getKind().isPrimitive())
                throw new IllegalArgumentException("component kind not primitive");
            this.ctx = ctx;
            this.type = type;
        }

        @Override
        public Primitive getComponentType() {
            return new Primitive(ctx, (PrimitiveType) type.getComponentType());
        }

        @Override
        public ArrayType getTypeMirror() {
            return type;
        }

        @Override
        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitPrimitiveArray(this);
        }
    }

    /** An array with declared elements. */
    public static class DeclaredArray extends Array {

        private final ProcessContext ctx;
        private final ArrayType type;

        DeclaredArray(ProcessContext ctx, ArrayType type) {
            if (!type.getComponentType().getKind().equals(TypeKind.DECLARED))
                throw new IllegalArgumentException("component kind not declared");
            this.ctx = ctx;
            this.type = type;
        }

        @Override
        public Declared getComponentType() {
            return new Declared(ctx, (DeclaredType) type.getComponentType());
        }

        @Override
        public ArrayType getTypeMirror() {
            return type;
        }

        @Override
        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitDeclaredArray(this);
        }
    }

    /** Anything else. {@link TypeKind#NULL} and {@link TypeKind#VOID} are special
     * cases of other. */
    public static class Other extends TypeInfo {

        private final TypeMirror type;

        Other(TypeMirror type) {
            this.type = type;
        }

        @Override
        public TypeMirror getTypeMirror() {
            return type;
        }

        @Override
        public String getCodeName() {
            return type.toString();
        }

        @Override
        public <R> R accept(Visitor<R> visitor) {
            switch (type.getKind()) {
            case VOID:
                return visitor.visitVoid(type);
            case NULL:
                return visitor.visitNull(type);
            default:
                return visitor.visitUnknown(type);
            }
        }
    }

    /**
     * A visitor for all kinds of type infos, and void and null.
     *
     * Default implementations are provided for all methods but {@link #visitUnknown(TypeMirror)}.
     * These implementations simply call {@link #visitUnknown(TypeMirror)}.
     *
     * @param <R> Return type, use Void if not needed.
     */
    public interface Visitor<R> {
        default R visitDeclared(Declared declared) {
            return visitUnknown(declared.getTypeMirror());
        }

        default R visitGeneric(Generic generic) {
            return visitUnknown(generic.getTypeMirror());
        }

        default R visitPrimitive(Primitive primitive) {
            return visitUnknown(primitive.getTypeMirror());
        }

        default R visitDeclaredArray(DeclaredArray array) {
            return visitUnknown(array.getTypeMirror());
        }

        default R visitPrimitiveArray(PrimitiveArray array) {
            return visitUnknown(array.getTypeMirror());
        }

        default R visitVoid(TypeMirror type) {
            return visitUnknown(type);
        }

        default R visitNull(TypeMirror type) {
            return visitUnknown(type);
        }

        R visitUnknown(TypeMirror type);
    }
}
